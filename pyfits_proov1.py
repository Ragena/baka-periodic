import pyfits
import sys
from math import sqrt

# This function looks at the difference between pixels. If it is smaller than the given cutoff, make them 0.
#def diff(img, cutoff):
#	for rida in img:
#		for i in range(len(rida)-1):
#			if abs(rida[i]-rida[i+1]) > cutoff:
#				rida[i] = abs(rida[i]-rida[i+1])
#			else:
#				rida[i] = 0;
#		rida[len(rida)-1] = 0
#
#	return img

def diff(img, size = 10):
	a = []
	avg = 0
	for rida in img:
		for i in range(len(rida)-1):
			v = []
			for j in a:
				v.append((avg - j)*(avg - j))
			if len(v) > 0:
				var = sum(v)/len(v)
			else:
				var = 0
			if len(a) >= size:
				del a[0]

			a.append(int(rida[i]))

			avg = sum(a)/len(a)

			if abs(a[-1] - avg) < 5 * int(sqrt(var)):
				rida[i] = 0

			

	return img

print("Opening file " + sys.argv[1])

img = 0

try:
	img = pyfits.getdata(sys.argv[1], ignore_missing_end = True)

except:
	print("Bad filename.") 
	exit(-1)


img = diff(img, 15)


#print(img)

print(pyfits.writeto(sys.argv[1]+"_sigma.fts", img, output_verify = "fix", clobber = True))
