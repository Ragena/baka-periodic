from sys import argv

csv_file = open(argv[1], "r+")

csv = csv_file.read().split("\n")

csv_file.seek(0) 
csv_file.readline()

for i in range(1, len(csv)-1):
	csv[i] = csv[i].split(",")
	try:
		x = round(abs(float(csv[i][0]) - float(csv[i][4])+1), 3)
		y = round(abs(float(csv[i][1]) - float(csv[i][5])+1), 3)
	except IndexError:
		x = 0
		y = 0
	csv[i].append(str(x))
	csv[i].append(str(y))
	csv_file.write(", ".join(csv[i]) + "\n")
	
csv_file.close()
