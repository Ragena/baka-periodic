import pyfits
from sys import argv
from math import sqrt
from myhdl import intbv

class Blob:
	def __init__(self, X, Y):
		self.size = 0
		self.sum = 0
		self.x_sum = 0
		self.y_sum = 0
		self.parts = [intbv(0, min = 0, max=2**Y) for j in range(X)]

	def add_to_blob(self, x, y, pix):
		if pix:
			self.size += 1
			self.sum += pix
			self.x_sum += x * pix
			self.y_sum += y * pix
			self.parts[x][y] = True

	def find_in_blob(self, x, y):
		try:
			return self.parts[x][y]
		except:
			return False
	def add_sigma(self, sigma):
		self.sigma=sigma

	def get(self):
		#if self.size > 4:
		return (round(float(self.x_sum)/self.sum, 3), round(float(self.y_sum)/self.sum, 3), self.sum, self.size)

M = 0
oM = 0
S = 0
k = 0
sigma = 0


def thresh(x, cutoff):
	global M
	global oM
	global S
	global k
	global sigma

	k += 1
	oM = M
	M = M - int((M - x) / k)
	if M < 0:
		M = 0
	S += (x - oM) * (x - M)

	if S > 0 and k > 1:
		sigma = int(sqrt(S/(k - 1)))
	else:
		sigma = 0

	if abs(x - M) < cutoff * sigma:
		x = 0
	else:
		x = x - M
	return x

print(argv[1])

img = 0

try:
	img = pyfits.getdata(argv[1], ignore_missing_end = True)

except:
	exit(-1)


pixel_buffer = [None for i in range(len(img[0]))]

blobs = []

GLOBAL_X = 0
GLOBAL_Y = 0

Last_active_blob = None
size = 0

for rida in img:
	for pixel in rida:
		pix = int(thresh(pixel, 5))
		if pix:
			if not Last_active_blob:
				for blob in blobs:
					if blob.find_in_blob(GLOBAL_X, GLOBAL_Y-1):
						Last_active_blob = blob
						break
			if Last_active_blob:
				for i in range(size):
					Last_active_blob.add_to_blob(GLOBAL_X - (i + 1), GLOBAL_Y, pixel_buffer[GLOBAL_X - (i + 1)])
				size = 0
				Last_active_blob.add_to_blob(GLOBAL_X, GLOBAL_Y, pix)

			else: # We didn't find a blob this could have been a part of.
				size += 1
		else: # The pixel in question DID NOT pass thresholding
			if size: # This pixel is preceeded by a light pixel. We should see if we can add it to a blob.
				if not Last_active_blob:
					Last_active_blob = Blob(len(img[0]), len(img))
					blobs.append(Last_active_blob)
				for i in range(size):
					Last_active_blob.add_to_blob(GLOBAL_X - (i + 1), GLOBAL_Y, pixel_buffer[GLOBAL_X - (i + 1)])
				Last_active_blob.add_sigma(sigma)
				size = 0
			else:
				Last_active_blob = None

		pixel_buffer[GLOBAL_X] = pix
		GLOBAL_X += 1
	GLOBAL_Y += 1
	GLOBAL_X = 0

out_file = open(argv[1]+ ".csv", "w")
out_file.write(argv[1]+ "\n")
for i in blobs:
	if i.get():
		txt = "{0[0]:.3f}, {0[1]:.3f}, {0[2]}, {0[3]}\n".format(i.get())
		out_file.write(txt)

