import pyfits
import sys
import numpy
import matplotlib.pyplot as plt

print("Opening file " + sys.argv[1])

img = 0

try:
	img = pyfits.getdata(sys.argv[1], ignore_missing_end = True)

except:
	print("Bad filename.")
	exit(-1)

print img

fft_img = numpy.empty(len(img), dtype=numpy.ndarray)

for rida in range(len(img)):
	fft_img[rida] = numpy.fft.fft(img[rida])[:400]

plt.plot(fft_img[1])
#plt.plot(abs(fft_img[1])[:200])
plt.show()



for rida in range(len(img)):
	img[rida] = numpy.fft.ifft(fft_img[rida])
#
#plt.plot(abs(n_img[1])[:len(n_img[1])/2])
#plt.plot(abs(n_img[1])[:200])
#plt.show()

#print(img)

pyfits.writeto(sys.argv[1]+"_dn.fts", abs(img), output_verify = "fix", clobber = True)
