library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

package Sensor is

    constant LENGTH_X : integer := 2592;
    constant LENGTH_Y : integer := 1944;
    constant MAX_BLOBS : integer := 10;

    type bit_map is array (0 to LENGTH_Y-1) of std_logic_vector (LENGTH_Y-1 downto 0);

    type blob is record
        b_map : bit_map;
        x_sum : unsigned (30 downto 0);
        y_sum : unsigned (30 downto 0);
        sum : unsigned (30 downto 0);
        n : integer range 0 to 127;
    end record;

    type blob_array is array (0 to MAX_BLOBS-1) of blob;
    type pixel_array is array (0 to LENGTH_X-1) of UNSIGNED (15 downto 0);

end Sensor;

package body Sensor is

end Sensor;
