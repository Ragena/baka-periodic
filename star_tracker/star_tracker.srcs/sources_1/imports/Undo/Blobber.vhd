library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;
use work.Sensor.ALL;

entity Blobber is
    Port ( in_pixel : in UNSIGNED (15 downto 0);
           clk_in : in STD_LOGIC;
           data_out : out STD_LOGIC);
end Blobber;

architecture Behavioral of Blobber is

    component threshold is
        Port ( clk_in : in std_logic;
               in_pix : in unsigned (15 downto 0);
               out_done : out std_logic;
               out_pix : out unsigned (15 downto 0));
    end component;

    signal pixel : UNSIGNED (15 downto 0);
    signal ready : STD_LOGIC;
    
    signal img_done : STD_LOGIC;
    
    signal p_array : PIXEL_ARRAY;
    signal b_array : BLOB_ARRAY;
    
    signal last_active_blob : integer range -1 to MAX_BLOBS-1 := -1;
    signal b_array_top : integer range 0 to MAX_BLOBS-1 := 0;
    
    signal blobless : integer range 0 to 20 := 0;
    signal x_index : integer range 0 to LENGTH_X-1;
    signal y_index : integer range 0 to LENGTH_Y-1; 
    
begin

    thresh : threshold PORT MAP(
        clk_in => clk_in,
        in_pix => in_pixel,
        out_done => ready,
        out_pix => pixel
    );


    proc : process (ready)
    begin
        if rising_edge(ready) then
        img_done <= '0';
            if pixel = "0000000000000000" then -- This pixel didn't pass the threshold.
                if blobless > 0 then
                    if last_active_blob = -1 then -- There is no "last active blob"
                        last_active_blob <= b_array_top; -- Assigning a new blob value as the "last active blob"
                        b_array_top <= b_array_top + 1; -- Increasing the array head.
                    end if;
                    
                    ADD_BLOBLESS_1: for i in 1 to 20 loop -- Adding each blobless pixel to the new blob.
                        if i > blobless then
                            exit ADD_BLOBLESS_1;
                        end if;
                        b_array(last_active_blob).b_map(y_index)(x_index - i) <= '1';
                        b_array(last_active_blob).x_sum <= b_array(last_active_blob).x_sum + ((x_index - i) * p_array(x_index - i));
                        b_array(last_active_blob).y_sum <= b_array(last_active_blob).y_sum + (y_index * p_array(x_index - i));
                        b_array(last_active_blob).sum <= b_array(last_active_blob).sum + p_array(x_index - i);
                        b_array(last_active_blob).n <= b_array(last_active_blob).n + 1;
                    end loop ADD_BLOBLESS_1;
                    blobless <= 0;
                else
                    last_active_blob <= -1;                    
                end if;
            else -- This pixel is not equal to zero, therefore passed the threshold.
                if last_active_blob = -1 then -- There is no "last active blob"
                    FIND_BLOB: for i in 0 to b_array_top-1 loop -- Search through the blob array, to find a blob this pixel can join.
                        if b_array(i).b_map(y_index - 1)(x_index) = '1' then
                            last_active_blob <= i;
                            exit FIND_BLOB;
                        end if;
                    end loop FIND_BLOB;
                    
                    if last_active_blob = -1 then -- The loop didn't find anything
                        blobless <= blobless + 1;
                    else
                        -- Add this pixel to the blob
                        b_array(last_active_blob).b_map(y_index)(x_index) <= '1';
                        b_array(last_active_blob).x_sum <= b_array(last_active_blob).x_sum + (x_index * pixel);
                        b_array(last_active_blob).y_sum <= b_array(last_active_blob).y_sum + (y_index * pixel);
                        b_array(last_active_blob).sum <= b_array(last_active_blob).sum + (pixel);
                        b_array(last_active_blob).n <= b_array(last_active_blob).n + 1;
                        
                        ADD_BLOBLESS_2: for i in 1 to 20 loop -- Adding each blobless pixel to the new blob.
                            if i > blobless then
                                exit ADD_BLOBLESS_2;
                            end if;
                            b_array(last_active_blob).b_map(y_index)(x_index - i) <= '1';
                            b_array(last_active_blob).x_sum <= b_array(last_active_blob).x_sum + ((x_index - i) * p_array(x_index - i));
                            b_array(last_active_blob).y_sum <= b_array(last_active_blob).y_sum + (y_index * p_array(x_index - i));
                            b_array(last_active_blob).sum <= b_array(last_active_blob).sum + p_array(x_index - i);
                            b_array(last_active_blob).n <= b_array(last_active_blob).n + 1;
                        end loop ADD_BLOBLESS_2;
                        blobless <= 0;
                    end if;
                end if;                
            end if;
            p_array(x_index) <= pixel;
            
            x_index <= x_index + 1;
            if x_index = 0 then
                y_index <= y_index + 1;
                if y_index = 0 then
                    img_done <= '1';
                    -- TODO: Reinitialize signals somewhere here.
                end if;
            end if;
        end if;
    end process;
    

end Behavioral;
