
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

package arith is

	function bin_sqrt ( num : in SIGNED) return SIGNED;
	function divide ( NUMERATOR,DENOMINATOR : in SIGNED) return SIGNED;

end arith;

package body arith is

	function bin_sqrt (num : in SIGNED ) return SIGNED is
		variable int_num : SIGNED (num'Length-1 downto 0);
		variable res : SIGNED (num'Length-1 downto 0) := (others => '0');
		variable bitt : SIGNED (num'Length-1 downto 0) := (0 => '1', others => '0');
	begin
		bitt := shift_left(bitt, bitt'Length-2);
		int_num := num;
		if (num < 0) then
			int_num := not int_num;
			int_num := int_num - 1;
		end if;
		L1: for i in 0 to 31 loop
			exit L1 when bitt <= num;
			bitt := shift_right(bitt, 2);
		end loop L1;
		
		L2: for i in 0 to num'Length-1 loop
			exit L2 when bitt = 0;
			if (int_num >= (res + bitt)) then
				int_num := int_num - (res + bitt);
				res := shift_right(res, 1);
				res := res + bitt;
			else
				res := shift_right(res, 1);
			end if;
			
			bitt := shift_right(bitt, 2);
		end loop L2;
		
		return res;
		
	end bin_sqrt;
		

		function divide ( NUMERATOR,DENOMINATOR : in SIGNED) return SIGNED is
			variable N : SIGNED (NUMERATOR'Length-1 downto 0) := (others => '0');
			variable D : SIGNED (DENOMINATOR'Length-1 downto 0) := (others => '0');
			variable Q : SIGNED (NUMERATOR'Length-1 downto 0) := (others => '0');
			variable R : SIGNED (DENOMINATOR'Length-1 downto 0) := (others => '0');
		begin
			N := NUMERATOR;
			if (N < 0) then
				N := not N;
				N := N - 1;
			end if;
			
			D := DENOMINATOR;
			if (D < 0) then
				D := not D;
				D := D - 1;
			end if;
			
			if (N = 0 or D = 0 or N < D) then
				return Q;
			end if;
			
			L: for i in 15 downto 0 loop
			
				Q := shift_left(Q, 1);
				R := shift_left(R, 1);
				
				R(0) := N(i);
				
				if (R >= D) then
					R := R - D;
					Q := Q + 1;
				end if;
				
			end loop L;
			
			if (NUMERATOR < 0 xor DENOMINATOR < 0) then
				Q := not Q;
				Q := Q + 1;
			end if;
			
			return Q;
			
		end divide;
		
end arith;
