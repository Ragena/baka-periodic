--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;
--use IEEE.STD_LOGIC_ARITH.all;
use IEEE.NUMERIC_STD.all;

package sqrt is

-- type <new_type> is
--  record
--    <type_name>        : UNSIGNED( 7 downto 0);
--    <type_name>        : std_logic;
-- end record;
--
-- Declare constants
--
-- constant <constant_name>		: time := <time_unit> ns;
-- constant <constant_name>		: UNSIGNED := <value;
--
-- Declare functions and procedure
--
--	function  bin_sqrt  ( d : UNSIGNED ) return UNSIGNED;
	function bin_sqrt ( num : in SIGNED) return SIGNED;
	function divide ( NUMERATOR,DENOMINATOR : in SIGNED) return SIGNED;
--	function stddev (signal x, M, S, k: in UNSIGNED) return UNSIGNED;
-- function <function_name>  (signal <signal_name> : in <type_declaration>) return <type_declaration>;
-- procedure <procedure_name> (<type_declaration> <constant_name>	: in <type_declaration>);
--

end sqrt;

package body sqrt is

--function  bin_sqrt  ( d : UNSIGNED ) return UNSIGNED is
--variable a : unsigned(31 downto 0):=d;  --original input.
--variable q : unsigned(15 downto 0):=(others => '0');  --result.
--variable left,right,r : unsigned(17 downto 0):=(others => '0');  --input to adder/sub.r-remainder.
--variable i : integer:=0;
--
--begin
--for i in 0 to 15 loop
--right(0):='1';
--right(1):=r(17);
--right(17 downto 2):=q;
--left(1 downto 0):=a(31 downto 30);
--left(17 downto 2):=r(15 downto 0);
--a(31 downto 2):=a(29 downto 0);  --shifting by 2 bit.
--if ( r(17) = '1') then
--r := left + right;
--else
--r := left - right;
--end if;
--q(15 downto 1) := q(14 downto 0);
--q(0) := not r(17);
--end loop;
--return q;
--
--end bin_sqrt;


	function bin_sqrt (num : in SIGNED ) return SIGNED is
		variable int_num : SIGNED (num'Length-1 downto 0);
		variable res : SIGNED (num'Length-1 downto 0) := (others => '0');
		variable bitt : SIGNED (num'Length-1 downto 0) := (0 => '1', others => '0');
	begin
		bitt := shift_left(bitt, bitt'Length-2);
		int_num := num;
		if (num < 0) then
			int_num := not int_num;
			int_num := int_num - 1;
		end if;
		L1: for i in 0 to 31 loop
			exit L1 when bitt <= num;
			bitt := shift_right(bitt, 2);
		end loop L1;
		
		L2: for i in 0 to num'Length-1 loop
			exit L2 when bitt = 0;
			if (int_num >= (res + bitt)) then
				int_num := int_num - (res + bitt);
				--num := int_num;
				res := shift_right(res, 1);
				res := res + bitt;
			else
				res := shift_right(res, 1);
			end if;
			
			bitt := shift_right(bitt, 2);
		end loop L2;
		
		return res;
		
	end bin_sqrt;
		
		function divide ( NUMERATOR,DENOMINATOR : in SIGNED) return SIGNED is
			--variable helper : STD_LOGIC_VECTOR (16 downto 0);
			variable N : SIGNED (NUMERATOR'Length-1 downto 0) := (others => '0');
			variable D : SIGNED (DENOMINATOR'Length-1 downto 0) := (others => '0');
			variable Q : SIGNED (NUMERATOR'Length-1 downto 0) := (others => '0');
			variable R : SIGNED (DENOMINATOR'Length-1 downto 0) := (others => '0');
		begin
			N := NUMERATOR;
			if (N < 0) then
				N := not N;
				N := N - 1;
			end if;
			
			D := DENOMINATOR;
			if (D < 0) then
				D := not D;
				D := D - 1;
			end if;
			
			if (N = 0 or D = 0 or N < D) then
				return Q;
			end if;
			
			L: for i in 15 downto 0 loop
			
				Q := shift_left(Q, 1);
				R := shift_left(R, 1);
				
				R(0) := N(i);
				
				if (R >= D) then
					R := R - D;
					Q := Q + 1;
				end if;
				
			end loop L;
			
			if (NUMERATOR < 0 xor DENOMINATOR < 0) then
				Q := not Q;
				Q := Q + 1;
			end if;
			
			return Q;
			
		end divide;
		
---- Example 1
--  function <function_name>  (signal <signal_name> : in <type_declaration>  ) return <type_declaration> is
--    variable <variable_name>     : <type_declaration>;
--  begin
--    <variable_name> := <signal_name> xor <signal_name>;
--    return <variable_name>; 
--  end <function_name>;

---- Example 2
--  function <function_name>  (signal <signal_name> : in <type_declaration>;
--                         signal <signal_name>   : in <type_declaration>  ) return <type_declaration> is
--  begin
--    if (<signal_name> = '1') then
--      return <signal_name>;
--    else
--      return 'Z';
--    end if;
--  end <function_name>;

---- Procedure Example
--  procedure <procedure_name>  (<type_declaration> <constant_name>  : in <type_declaration>) is
--    
--  begin
--    
--  end <procedure_name>;
 
end sqrt;
