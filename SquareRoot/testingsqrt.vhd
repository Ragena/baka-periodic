-- TestBench Template 

  LIBRARY ieee;
  USE ieee.std_logic_1164.ALL;
  --use IEEE.STD_LOGIC_ARITH.all;
  USE ieee.numeric_std.ALL;
  USE work.sqrt.ALL;

  ENTITY testbench IS
  END testbench;

  ARCHITECTURE behavior OF testbench IS 

--  -- Component Declaration
--          COMPONENT binary_sqrt
--          PORT(
--                  sqr : in  INTEGER range 0 to 65535;
--						en : in std_logic;
--						root : out  INTEGER range 0 to 65535
--                  );
--          END COMPONENT;
--
          SIGNAL sqr : unsigned(15 downto 0);
          SIGNAL root : unsigned(15 downto 0);
--          SIGNAL en: std_logic := '0';

  BEGIN

  -- Component Instantiation
--          uut: binary_sqrt PORT MAP(
--                  sqr => sqr,
--						en => en,
--                  root => root
--          );


  --  Test Bench Statements
     tb : PROCESS
     BEGIN

        wait for 100 ns; -- wait until global set/reset completes

        -- Add user defined stimulus here

		  L: for i in 0 to 65535 loop
			sqr <= to_unsigned(i, 16);
			root <= bin_sqrt(sqr);
			end loop L;
--		sqr <= 4;
--		en <= '1';
--		wait for 10 ns;
--		en <= '0';
--		wait for 1000 ns;
--		
--		sqr <= 36;
--		en <= '1';
--		wait for 10 ns;
--		en <= '0';
--		wait for 1000 ns;
--
--		sqr <= 128;
--		en <= '1';
--		wait for 10 ns;
--		en <= '0';
--		wait for 1000 ns;

        wait; -- will wait forever
     END PROCESS tb;
  --  End Test Bench 

  END;
