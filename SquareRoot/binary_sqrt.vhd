----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:52:23 05/01/2016 
-- Design Name: 
-- Module Name:    binary_sqrt - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
--use IEEE.STD_LOGIC_ARITH.all;
use work.sqrt.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity binary_sqrt is
    Port ( clk_in : in std_logic;
			  in_pix : in unsigned (15 downto 0);
			  out_done : out std_logic;
           out_pix : out unsigned (15 downto 0));
end binary_sqrt;

architecture Behavioral of binary_sqrt is

signal pixel : unsigned (15 downto 0) := (others => '0');

signal done : std_logic := '0';
signal new_pix : std_logic := '0';

begin

	thresh : process (new_pix)

	variable oM : signed (16 downto 0);
	variable M : signed (16 downto 0);
	variable S : signed (33 downto 0);
	variable k : signed (21 downto 0);
	variable sigma : signed (16 downto 0);
	variable sigma_x_5 : signed (16 downto 0);
	variable x : signed (16 downto 0);
	variable diff : signed (16 downto 0);
	variable odiff : signed (16 downto 0);
	
	variable helper : std_logic_vector(15 downto 0) := (others => '0');
	
	begin
		if rising_edge(new_pix) then
			done <= '0';
			helper := std_logic_vector(in_pix);
			x(15 downto 0) := signed(helper);
			
			k := k + 1;
			oM := M;
			diff := x - M;
			M := M + divide(diff, k);
			
			odiff := x - oM;
			
			S := S + odiff*diff;
			S := divide(S, (k-1));
			sigma := bin_sqrt(S)(16 downto 0);
			sigma_x_5 := shift_left(sigma, 2);
			sigma_x_5 := sigma_x_5 + sigma;
			
			
			
			if diff < sigma_x_5 then
				pixel <= (others => '0');
			else
				pixel <= in_pix(15 downto 0);
			end if;
			done <= '1';
		end if;
	end process;
		
	new_pixel : process (clk_in)
		variable pix : unsigned (15 downto 0) := (others => '0');
	begin
		if rising_edge(clk_in) then
			if (pix = in_pix) then
				new_pix <= '0';
			else
				pix := in_pix;
				new_pix <= '1';
			end if;
		end if;
	end process;

out_pix <= pixel;
out_done <= done;
--root <= bin_sqrt(sqr)(7 downto 0) when rising_edge(en);


end Behavioral;

