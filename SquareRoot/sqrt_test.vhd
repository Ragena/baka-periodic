-- TestBench Template 

  LIBRARY ieee;
  USE ieee.std_logic_1164.ALL;
  use IEEE.STD_LOGIC_ARITH.all;
  USE ieee.numeric_std.ALL;
  USE sqrt.ALL;

  ENTITY testbench IS
  END testbench;

  ARCHITECTURE behavior OF testbench IS 

          SIGNAL sqr : integer range 0 to 65535;
          SIGNAL root : integer range 0 to 65535;
          

  BEGIN

  --  Test Bench Statements
     tb : PROCESS
     BEGIN
			L: while sqr < 65535 loop
			
			root <= sqrt(sqr);
			
			wait for 100 ns; -- wait until global set/reset completes
			end loop L;
		  

        -- Add user defined stimulus here

        wait; -- will wait forever
     END PROCESS tb;
  --  End Test Bench 

  END;
