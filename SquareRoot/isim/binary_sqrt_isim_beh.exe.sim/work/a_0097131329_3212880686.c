/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/andu/baka/SquareRoot/binary_sqrt.vhd";
extern char *IEEE_P_2592010699;
extern char *IEEE_P_1242562249;

char *ieee_p_1242562249_sub_10420449594411817395_1035706684(char *, char *, int , int );
int ieee_p_1242562249_sub_17802405650254020620_1035706684(char *, char *, char *);
char *ieee_p_1242562249_sub_875668032861425848_1035706684(char *, char *, char *, char *, int );
unsigned char ieee_p_2592010699_sub_2763492388968962707_503743352(char *, char *, unsigned int , unsigned int );


static void work_a_0097131329_3212880686_p_0(char *t0)
{
    char t11[16];
    char t12[16];
    char *t1;
    unsigned char t2;
    char *t3;
    char *t4;
    int t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    int t10;
    int t13;
    int t14;
    char *t15;
    char *t16;
    char *t17;
    int t18;
    unsigned char t19;
    int t20;
    int t21;
    int t22;
    int t23;
    int t24;
    char *t25;
    char *t26;

LAB0:    xsi_set_current_line(50, ng0);
    t1 = (t0 + 1152U);
    t2 = ieee_p_2592010699_sub_2763492388968962707_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 3712);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(51, ng0);
    t3 = (t0 + 1032U);
    t4 = *((char **)t3);
    t5 = *((int *)t4);
    t3 = (t0 + 3808);
    t6 = (t3 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    *((int *)t9) = t5;
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(53, ng0);

LAB5:    t1 = (t0 + 1992U);
    t3 = *((char **)t1);
    t5 = *((int *)t3);
    t1 = (t0 + 1032U);
    t4 = *((char **)t1);
    t10 = *((int *)t4);
    t2 = (t5 > t10);
    if (t2 != 0)
        goto LAB6;

LAB8:    xsi_set_current_line(57, ng0);

LAB9:    t1 = (t0 + 1992U);
    t3 = *((char **)t1);
    t5 = *((int *)t3);
    t2 = (t5 > 0);
    if (t2 != 0)
        goto LAB10;

LAB12:    xsi_set_current_line(70, ng0);
    t1 = (t0 + 1832U);
    t3 = *((char **)t1);
    t5 = *((int *)t3);
    t1 = (t0 + 4000);
    t4 = (t1 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((int *)t8) = t5;
    xsi_driver_first_trans_fast(t1);
    goto LAB3;

LAB6:    xsi_set_current_line(54, ng0);
    t1 = (t0 + 1992U);
    t6 = *((char **)t1);
    t13 = *((int *)t6);
    t1 = ieee_p_1242562249_sub_10420449594411817395_1035706684(IEEE_P_1242562249, t12, t13, 16);
    t7 = ieee_p_1242562249_sub_875668032861425848_1035706684(IEEE_P_1242562249, t11, t1, t12, 2);
    t14 = ieee_p_1242562249_sub_17802405650254020620_1035706684(IEEE_P_1242562249, t7, t11);
    t8 = (t0 + 3872);
    t9 = (t8 + 56U);
    t15 = *((char **)t9);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    *((int *)t17) = t14;
    xsi_driver_first_trans_fast(t8);
    goto LAB5;

LAB7:;
LAB10:    xsi_set_current_line(58, ng0);
    t1 = (t0 + 1512U);
    t4 = *((char **)t1);
    t10 = *((int *)t4);
    t1 = (t0 + 1832U);
    t6 = *((char **)t1);
    t13 = *((int *)t6);
    t1 = (t0 + 1992U);
    t7 = *((char **)t1);
    t14 = *((int *)t7);
    t18 = (t13 + t14);
    t19 = (t10 >= t18);
    if (t19 != 0)
        goto LAB13;

LAB15:    xsi_set_current_line(64, ng0);
    t1 = (t0 + 1832U);
    t3 = *((char **)t1);
    t5 = *((int *)t3);
    t1 = ieee_p_1242562249_sub_10420449594411817395_1035706684(IEEE_P_1242562249, t12, t5, 16);
    t4 = ieee_p_1242562249_sub_875668032861425848_1035706684(IEEE_P_1242562249, t11, t1, t12, 1);
    t10 = ieee_p_1242562249_sub_17802405650254020620_1035706684(IEEE_P_1242562249, t4, t11);
    t6 = (t0 + 3936);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t15 = *((char **)t9);
    *((int *)t15) = t10;
    xsi_driver_first_trans_fast(t6);

LAB14:    xsi_set_current_line(67, ng0);
    t1 = (t0 + 1992U);
    t3 = *((char **)t1);
    t5 = *((int *)t3);
    t1 = ieee_p_1242562249_sub_10420449594411817395_1035706684(IEEE_P_1242562249, t12, t5, 16);
    t4 = ieee_p_1242562249_sub_875668032861425848_1035706684(IEEE_P_1242562249, t11, t1, t12, 2);
    t10 = ieee_p_1242562249_sub_17802405650254020620_1035706684(IEEE_P_1242562249, t4, t11);
    t6 = (t0 + 3872);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t15 = *((char **)t9);
    *((int *)t15) = t10;
    xsi_driver_first_trans_fast(t6);
    goto LAB9;

LAB11:;
LAB13:    xsi_set_current_line(59, ng0);
    t1 = (t0 + 1512U);
    t8 = *((char **)t1);
    t20 = *((int *)t8);
    t1 = (t0 + 1832U);
    t9 = *((char **)t1);
    t21 = *((int *)t9);
    t1 = (t0 + 1992U);
    t15 = *((char **)t1);
    t22 = *((int *)t15);
    t23 = (t21 + t22);
    t24 = (t20 - t23);
    t1 = (t0 + 3808);
    t16 = (t1 + 56U);
    t17 = *((char **)t16);
    t25 = (t17 + 56U);
    t26 = *((char **)t25);
    *((int *)t26) = t24;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(61, ng0);
    t1 = (t0 + 1832U);
    t3 = *((char **)t1);
    t5 = *((int *)t3);
    t1 = ieee_p_1242562249_sub_10420449594411817395_1035706684(IEEE_P_1242562249, t12, t5, 16);
    t4 = ieee_p_1242562249_sub_875668032861425848_1035706684(IEEE_P_1242562249, t11, t1, t12, 1);
    t10 = ieee_p_1242562249_sub_17802405650254020620_1035706684(IEEE_P_1242562249, t4, t11);
    t6 = (t0 + 3936);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t15 = *((char **)t9);
    *((int *)t15) = t10;
    xsi_driver_first_trans_fast(t6);
    xsi_set_current_line(62, ng0);
    t1 = (t0 + 1832U);
    t3 = *((char **)t1);
    t5 = *((int *)t3);
    t1 = (t0 + 1992U);
    t4 = *((char **)t1);
    t10 = *((int *)t4);
    t13 = (t5 + t10);
    t1 = (t0 + 3936);
    t6 = (t1 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    *((int *)t9) = t13;
    xsi_driver_first_trans_fast(t1);
    goto LAB14;

}

static void work_a_0097131329_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    int t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;

LAB0:    xsi_set_current_line(75, ng0);

LAB3:    t1 = (t0 + 1672U);
    t2 = *((char **)t1);
    t3 = *((int *)t2);
    t1 = (t0 + 4064);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((int *)t7) = t3;
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t8 = (t0 + 3728);
    *((int *)t8) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}


extern void work_a_0097131329_3212880686_init()
{
	static char *pe[] = {(void *)work_a_0097131329_3212880686_p_0,(void *)work_a_0097131329_3212880686_p_1};
	xsi_register_didat("work_a_0097131329_3212880686", "isim/binary_sqrt_isim_beh.exe.sim/work/a_0097131329_3212880686.didat");
	xsi_register_executes(pe);
}
