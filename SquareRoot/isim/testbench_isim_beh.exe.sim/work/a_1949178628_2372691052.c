/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/andu/baka/SquareRoot/testingsqrt.vhd";
extern char *IEEE_P_1242562249;
extern char *WORK_P_3921294355;

char *ieee_p_1242562249_sub_10420449594411817395_1035706684(char *, char *, int , int );
char *work_p_3921294355_sub_8851409917039704590_317124355(char *, char *, char *, unsigned int , unsigned int );


static void work_a_1949178628_2372691052_p_0(char *t0)
{
    char t7[16];
    char *t1;
    char *t2;
    int64 t3;
    char *t4;
    int t5;
    int t6;
    char *t8;
    char *t9;
    char *t10;
    unsigned int t11;
    unsigned char t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    int t18;

LAB0:    t1 = (t0 + 2344U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(41, ng0);
    t3 = (100 * 1000LL);
    t2 = (t0 + 2152);
    xsi_process_wait(t2, t3);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(45, ng0);
    t2 = (t0 + 4360);
    *((int *)t2) = 0;
    t4 = (t0 + 4364);
    *((int *)t4) = 65535;
    t5 = 0;
    t6 = 65535;

LAB8:    if (t5 <= t6)
        goto LAB9;

LAB11:    xsi_set_current_line(67, ng0);

LAB19:    *((char **)t1) = &&LAB20;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB9:    xsi_set_current_line(46, ng0);
    t8 = (t0 + 4360);
    t9 = ieee_p_1242562249_sub_10420449594411817395_1035706684(IEEE_P_1242562249, t7, *((int *)t8), 16);
    t10 = (t7 + 12U);
    t11 = *((unsigned int *)t10);
    t11 = (t11 * 1U);
    t12 = (16U != t11);
    if (t12 == 1)
        goto LAB12;

LAB13:    t13 = (t0 + 2728);
    t14 = (t13 + 56U);
    t15 = *((char **)t14);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    memcpy(t17, t9, 16U);
    xsi_driver_first_trans_fast(t13);
    xsi_set_current_line(47, ng0);
    t2 = (t0 + 992U);
    t4 = work_p_3921294355_sub_8851409917039704590_317124355(WORK_P_3921294355, t7, t2, 0U, 0U);
    t8 = (t7 + 12U);
    t11 = *((unsigned int *)t8);
    t11 = (t11 * 1U);
    t12 = (16U != t11);
    if (t12 == 1)
        goto LAB14;

LAB15:    t9 = (t0 + 2792);
    t10 = (t9 + 56U);
    t13 = *((char **)t10);
    t14 = (t13 + 56U);
    t15 = *((char **)t14);
    memcpy(t15, t4, 16U);
    xsi_driver_first_trans_fast(t9);

LAB10:    t2 = (t0 + 4360);
    t5 = *((int *)t2);
    t4 = (t0 + 4364);
    t6 = *((int *)t4);
    if (t5 == t6)
        goto LAB11;

LAB16:    t18 = (t5 + 1);
    t5 = t18;
    t8 = (t0 + 4360);
    *((int *)t8) = t5;
    goto LAB8;

LAB12:    xsi_size_not_matching(16U, t11, 0);
    goto LAB13;

LAB14:    xsi_size_not_matching(16U, t11, 0);
    goto LAB15;

LAB17:    goto LAB2;

LAB18:    goto LAB17;

LAB20:    goto LAB18;

}


extern void work_a_1949178628_2372691052_init()
{
	static char *pe[] = {(void *)work_a_1949178628_2372691052_p_0};
	xsi_register_didat("work_a_1949178628_2372691052", "isim/testbench_isim_beh.exe.sim/work/a_1949178628_2372691052.didat");
	xsi_register_executes(pe);
}
