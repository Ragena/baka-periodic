/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
extern char *IEEE_P_1242562249;

unsigned char ieee_p_1242562249_sub_1434220770695818471_1035706684(char *, char *, char *, char *, char *);
unsigned char ieee_p_1242562249_sub_1434220770698190313_1035706684(char *, char *, char *, char *, char *);
char *ieee_p_1242562249_sub_1701011461141717515_1035706684(char *, char *, char *, char *, char *, char *);
char *ieee_p_1242562249_sub_1701011461141789389_1035706684(char *, char *, char *, char *, char *, char *);
unsigned char ieee_p_1242562249_sub_3307759752501503797_1035706684(char *, char *, char *, int );
char *ieee_p_1242562249_sub_8645934262925994370_1035706684(char *, char *, char *, char *, int );


char *work_p_3921294355_sub_8851409917039704590_317124355(char *t1, char *t2, char *t3, unsigned int t4, unsigned int t5)
{
    char t6[368];
    char t8[16];
    char t13[16];
    char t18[16];
    char t24[16];
    char t30[16];
    char t39[16];
    char t46[16];
    char t55[16];
    char t56[16];
    char t57[16];
    char *t0;
    char *t9;
    char *t10;
    int t11;
    unsigned int t12;
    char *t14;
    int t15;
    char *t16;
    char *t17;
    char *t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t25;
    char *t26;
    int t27;
    char *t28;
    char *t29;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;
    int t36;
    unsigned int t37;
    char *t38;
    char *t40;
    char *t41;
    int t42;
    unsigned int t43;
    char *t44;
    char *t45;
    char *t47;
    char *t48;
    char *t49;
    char *t50;
    char *t51;
    char *t52;
    char *t53;
    unsigned char t54;

LAB0:    t9 = (t8 + 0U);
    t10 = (t9 + 0U);
    *((int *)t10) = 15;
    t10 = (t9 + 4U);
    *((int *)t10) = 0;
    t10 = (t9 + 8U);
    *((int *)t10) = -1;
    t11 = (0 - 15);
    t12 = (t11 * -1);
    t12 = (t12 + 1);
    t10 = (t9 + 12U);
    *((unsigned int *)t10) = t12;
    t10 = (t13 + 0U);
    t14 = (t10 + 0U);
    *((int *)t14) = 15;
    t14 = (t10 + 4U);
    *((int *)t14) = 0;
    t14 = (t10 + 8U);
    *((int *)t14) = -1;
    t15 = (0 - 15);
    t12 = (t15 * -1);
    t12 = (t12 + 1);
    t14 = (t10 + 12U);
    *((unsigned int *)t14) = t12;
    t14 = (t6 + 4U);
    t16 = ((IEEE_P_1242562249) + 2976);
    t17 = (t14 + 88U);
    *((char **)t17) = t16;
    t19 = (t14 + 56U);
    *((char **)t19) = t18;
    xsi_type_set_default_value(t16, t18, t13);
    t20 = (t14 + 64U);
    *((char **)t20) = t13;
    t21 = (t14 + 80U);
    *((unsigned int *)t21) = 16U;
    t22 = xsi_get_transient_memory(16U);
    memset(t22, 0, 16U);
    t23 = t22;
    memset(t23, (unsigned char)2, 16U);
    t25 = (t24 + 0U);
    t26 = (t25 + 0U);
    *((int *)t26) = 15;
    t26 = (t25 + 4U);
    *((int *)t26) = 0;
    t26 = (t25 + 8U);
    *((int *)t26) = -1;
    t27 = (0 - 15);
    t12 = (t27 * -1);
    t12 = (t12 + 1);
    t26 = (t25 + 12U);
    *((unsigned int *)t26) = t12;
    t26 = (t6 + 124U);
    t28 = ((IEEE_P_1242562249) + 2976);
    t29 = (t26 + 88U);
    *((char **)t29) = t28;
    t31 = (t26 + 56U);
    *((char **)t31) = t30;
    memcpy(t30, t22, 16U);
    t32 = (t26 + 64U);
    *((char **)t32) = t24;
    t33 = (t26 + 80U);
    *((unsigned int *)t33) = 16U;
    t34 = xsi_get_transient_memory(16U);
    memset(t34, 0, 16U);
    t35 = t34;
    memset(t35, (unsigned char)2, 16U);
    t36 = (14 - 15);
    t12 = (t36 * -1);
    t37 = (1U * t12);
    t38 = (t35 + t37);
    *((unsigned char *)t38) = (unsigned char)3;
    t40 = (t39 + 0U);
    t41 = (t40 + 0U);
    *((int *)t41) = 15;
    t41 = (t40 + 4U);
    *((int *)t41) = 0;
    t41 = (t40 + 8U);
    *((int *)t41) = -1;
    t42 = (0 - 15);
    t43 = (t42 * -1);
    t43 = (t43 + 1);
    t41 = (t40 + 12U);
    *((unsigned int *)t41) = t43;
    t41 = (t6 + 244U);
    t44 = ((IEEE_P_1242562249) + 2976);
    t45 = (t41 + 88U);
    *((char **)t45) = t44;
    t47 = (t41 + 56U);
    *((char **)t47) = t46;
    memcpy(t46, t34, 16U);
    t48 = (t41 + 64U);
    *((char **)t48) = t39;
    t49 = (t41 + 80U);
    *((unsigned int *)t49) = 16U;
    t50 = (t3 + 40U);
    t51 = *((char **)t50);
    t50 = (t51 + t5);
    t51 = (t14 + 56U);
    t52 = *((char **)t51);
    t51 = (t52 + 0);
    t53 = (t8 + 12U);
    t43 = *((unsigned int *)t53);
    t43 = (t43 * 1U);
    memcpy(t51, t50, t43);
    t11 = 0;
    t15 = 7;

LAB2:    if (t11 <= t15)
        goto LAB3;

LAB5:    t11 = 0;
    t15 = 7;

LAB8:    if (t11 <= t15)
        goto LAB9;

LAB11:    t9 = (t26 + 56U);
    t10 = *((char **)t9);
    t9 = (t24 + 12U);
    t12 = *((unsigned int *)t9);
    t12 = (t12 * 1U);
    t0 = xsi_get_transient_memory(t12);
    memcpy(t0, t10, t12);
    t16 = (t24 + 0U);
    t11 = *((int *)t16);
    t17 = (t24 + 4U);
    t15 = *((int *)t17);
    t19 = (t24 + 8U);
    t27 = *((int *)t19);
    t20 = (t2 + 0U);
    t21 = (t20 + 0U);
    *((int *)t21) = t11;
    t21 = (t20 + 4U);
    *((int *)t21) = t15;
    t21 = (t20 + 8U);
    *((int *)t21) = t27;
    t36 = (t15 - t11);
    t37 = (t36 * t27);
    t37 = (t37 + 1);
    t21 = (t20 + 12U);
    *((unsigned int *)t21) = t37;

LAB1:    return t0;
LAB3:    t9 = (t41 + 56U);
    t10 = *((char **)t9);
    t9 = (t3 + 40U);
    t16 = *((char **)t9);
    t9 = (t16 + t5);
    t54 = ieee_p_1242562249_sub_1434220770695818471_1035706684(IEEE_P_1242562249, t10, t39, t9, t8);
    if (t54 != 0)
        goto LAB5;

LAB6:    t9 = (t41 + 56U);
    t10 = *((char **)t9);
    t9 = ieee_p_1242562249_sub_8645934262925994370_1035706684(IEEE_P_1242562249, t55, t10, t39, 2);
    t16 = (t41 + 56U);
    t17 = *((char **)t16);
    t16 = (t17 + 0);
    t19 = (t55 + 12U);
    t12 = *((unsigned int *)t19);
    t12 = (t12 * 1U);
    memcpy(t16, t9, t12);

LAB4:    if (t11 == t15)
        goto LAB5;

LAB7:    t27 = (t11 + 1);
    t11 = t27;
    goto LAB2;

LAB9:    t9 = (t41 + 56U);
    t10 = *((char **)t9);
    t54 = ieee_p_1242562249_sub_3307759752501503797_1035706684(IEEE_P_1242562249, t10, t39, 0);
    if (t54 != 0)
        goto LAB11;

LAB12:    t9 = (t14 + 56U);
    t10 = *((char **)t9);
    t9 = (t26 + 56U);
    t16 = *((char **)t9);
    t9 = (t41 + 56U);
    t17 = *((char **)t9);
    t9 = ieee_p_1242562249_sub_1701011461141717515_1035706684(IEEE_P_1242562249, t55, t16, t24, t17, t39);
    t54 = ieee_p_1242562249_sub_1434220770698190313_1035706684(IEEE_P_1242562249, t10, t13, t9, t55);
    if (t54 != 0)
        goto LAB13;

LAB15:    t9 = (t26 + 56U);
    t10 = *((char **)t9);
    t9 = ieee_p_1242562249_sub_8645934262925994370_1035706684(IEEE_P_1242562249, t55, t10, t24, 1);
    t16 = (t26 + 56U);
    t17 = *((char **)t16);
    t16 = (t17 + 0);
    t19 = (t55 + 12U);
    t12 = *((unsigned int *)t19);
    t12 = (t12 * 1U);
    memcpy(t16, t9, t12);

LAB14:    t9 = (t41 + 56U);
    t10 = *((char **)t9);
    t9 = ieee_p_1242562249_sub_8645934262925994370_1035706684(IEEE_P_1242562249, t55, t10, t39, 2);
    t16 = (t41 + 56U);
    t17 = *((char **)t16);
    t16 = (t17 + 0);
    t19 = (t55 + 12U);
    t12 = *((unsigned int *)t19);
    t12 = (t12 * 1U);
    memcpy(t16, t9, t12);

LAB10:    if (t11 == t15)
        goto LAB11;

LAB16:    t27 = (t11 + 1);
    t11 = t27;
    goto LAB8;

LAB13:    t19 = (t14 + 56U);
    t20 = *((char **)t19);
    t19 = (t26 + 56U);
    t21 = *((char **)t19);
    t19 = (t41 + 56U);
    t22 = *((char **)t19);
    t19 = ieee_p_1242562249_sub_1701011461141717515_1035706684(IEEE_P_1242562249, t57, t21, t24, t22, t39);
    t23 = ieee_p_1242562249_sub_1701011461141789389_1035706684(IEEE_P_1242562249, t56, t20, t13, t19, t57);
    t25 = (t14 + 56U);
    t28 = *((char **)t25);
    t25 = (t28 + 0);
    t29 = (t56 + 12U);
    t12 = *((unsigned int *)t29);
    t37 = (1U * t12);
    memcpy(t25, t23, t37);
    t9 = (t26 + 56U);
    t10 = *((char **)t9);
    t9 = ieee_p_1242562249_sub_8645934262925994370_1035706684(IEEE_P_1242562249, t55, t10, t24, 1);
    t16 = (t26 + 56U);
    t17 = *((char **)t16);
    t16 = (t17 + 0);
    t19 = (t55 + 12U);
    t12 = *((unsigned int *)t19);
    t12 = (t12 * 1U);
    memcpy(t16, t9, t12);
    t9 = (t26 + 56U);
    t10 = *((char **)t9);
    t9 = (t41 + 56U);
    t16 = *((char **)t9);
    t9 = ieee_p_1242562249_sub_1701011461141717515_1035706684(IEEE_P_1242562249, t55, t10, t24, t16, t39);
    t17 = (t26 + 56U);
    t19 = *((char **)t17);
    t17 = (t19 + 0);
    t20 = (t55 + 12U);
    t12 = *((unsigned int *)t20);
    t37 = (1U * t12);
    memcpy(t17, t9, t37);
    goto LAB14;

LAB17:;
}


extern void work_p_3921294355_init()
{
	static char *se[] = {(void *)work_p_3921294355_sub_8851409917039704590_317124355};
	xsi_register_didat("work_p_3921294355", "isim/testbench_isim_beh.exe.sim/work/p_3921294355.didat");
	xsi_register_subprogram_executes(se);
}
