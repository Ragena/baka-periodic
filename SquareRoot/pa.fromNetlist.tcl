
# PlanAhead Launch Script for Post-Synthesis floorplanning, created by Project Navigator

create_project -name SquareRoot -dir "/home/andu/baka/SquareRoot/planAhead_run_2" -part xc3sd1800afg676-4
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "/home/andu/baka/SquareRoot/binary_sqrt.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {/home/andu/baka/SquareRoot} }
set_property target_constrs_file "binary_sqrt.ucf" [current_fileset -constrset]
add_files [list {binary_sqrt.ucf}] -fileset [get_property constrset [current_run]]
link_design
