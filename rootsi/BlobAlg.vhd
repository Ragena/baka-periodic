
--BlobAlg.vhd
--
--Thismoduleisrunningtheblobdetectionalgorithminfourflag-synchronizedstatemachines.Itistobeusedintheminiaturestartracker.
--Thealorithmtakestheimagefromthecamera,detectsstars/blobs,groupsthemtogetherandoutputssimple(X,Y,I)-dataforeachstar.
--
--Eachstatemachinecanbeseenasrepresentingalayerinthealgorithminwhichthefirstone(layer1)isindirectcontactwiththepixeldatafromthesensor.Lowlevel
--Thefourthstatemachine(layer4)iseventuallyputtingthedetectedstars(X,Y,I)intotheSPIbuffer.Highestlevel
--
--2014-05-16
--MarcusLindh(marculin@kth.se)
--073-9786602

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;

entity BlobAlg is
	generic(SIZE: INTEGER := 24);
	port(
			CLK,Reset : in std_logic;
			Enable : in std_logic;
			FrameValid : in std_logic;
			LineValid : in std_logic;
			Data_Camera : in std_logic_vector(11 DOWNTO 0);
			FIFO_in : in std_logic_vector(63 DOWNTO 0);
			FIFO_empty : in std_logic;
			Threshold_in : in std_logic_vector(15 DOWNTO 0);
			Threshold_refresh : in std_logic;
			New_Blob_Detected : out std_logic;
			FIFO_we : out std_logic;
			FIFO_re : out std_logic;
			Blob_Data_Out : out std_logic_vector(31 DOWNTO 0);
			FIFO_out : out std_logic_vector(63 DOWNTO 0));
end BlobAlg;

architecture behavioral of BlobAlg is
---SIGNAL declaration
SIGNAL Threshold_Value : std_logic_vector(11 DOWNTO 0);
--thresholdingvalue
SIGNAL Threshold_refresh_old : std_logic;

--Blob algorithm SIGNALs
SIGNAL Ghosting : std_logic_vector(7 DOWNTO 0);

CONSTANT Ghosting_Level : std_logic_vector(7 DOWNTO 0) := x"19"; --SetstheGhostingLevelto25

SIGNAL Blobber_State : std_logic_vector(3 DOWNTO 0);
SIGNAL Data_Prepare_State : std_logic_vector(3 DOWNTO 0);
SIGNAL Release_State : std_logic_vector(3 DOWNTO 0);

--SIGNALs belonging to the statemachine integrating blob related pixels
SIGNAL XI_sum : std_logic_vector(23 DOWNTO 0);
SIGNAL I_sum : std_logic_vector(23 DOWNTO 0);
SIGNAL Blob_Width : std_logic_vector(7 DOWNTO 0);

--signals sent from the integrating 
SIGNAL XI_sum_New : std_logic_vector(23 DOWNTO 0);
SIGNAL I_sum_nEW : std_logic_vector(23 DOWNTO 0);
SIGNAL X_center : std_logic_vector(11 DOWNTO 0);
SIGNAL Blob_Row : std_logic_vector(11 DOWNTO 0);

SIGNAL RowBlob_Start_Column_New : std_logic_vector(11 DOWNTO 0);
SIGNAL RowBlob_Start_Column : std_logic_vector(11 DOWNTO 0);

SIGNAL New_Blob_To_Calculate : std_logic;

SIGNAL New_Frame : std_logic;

--Datarelatedtotheblobatfirst
SIGNAL FIFO1_X_center : std_logic_vector(11 DOWNTO 0);
SIGNAL FIFO1_X_center_Sum : std_logic_vector(23 DOWNTO 0);
SIGNAL FIFO1_Rowspan : std_logic_vector(7 DOWNTO 0);
SIGNAL FIFO1_IrowN_sum : std_logic_vector(23 DOWNTO 0);
SIGNAL FIFO1_Irow_sum : std_logic_vector(23 DOWNTO 0);
SIGNAL FIFO1_Y_stopp : std_logic_vector(11 DOWNTO 0);
SIGNAL FIFO1_PXcount : std_logic_vector(9 DOWNTO 0);

--
SIGNAL FIFO1_Data_Snatched : std_logic;
SIGNAL FIFO1_Temp1 : std_logic_vector(63 DOWNTO 0);
SIGNAL FIFO1_Temp2 : std_logic_vector(63 DOWNTO 0);

SIGNAL FIFO1_Conditioner_State : std_logic_vector(3 DOWNTO 0);


SIGNAL RowBlob_X_center : std_logic_vector(11 DOWNTO 0);
SIGNAL RowBlob_X_center_Sum : std_logic_vector(23 DOWNTO 0);
SIGNAL RowBlob_Irow_sum : std_logic_vector(23 DOWNTO 0);
SIGNAL RowBlob_IrowN_sum : std_logic_vector(31 DOWNTO 0);
SIGNAL RowBlob_Row : std_logic_vector(11 DOWNTO 0);
SIGNAL RowBlob_PXcount : std_logic_vector(11 DOWNTO 0);
SIGNAL RowBlob_NewFlag : std_logic;
SIGNAL RowBlob_Rowspan : std_logic_vector(7 DOWNTO 0);


SIGNAL Tmp1_message1 : std_logic_vector(63 DOWNTO 0);
SIGNAL Tmp1_message2 : std_logic_vector(63 DOWNTO 0);


SIGNAL Tmp2_message1 : std_logic_vector(63 DOWNTO 0);
SIGNAL Tmp2_message2 : std_logic_vector(63 DOWNTO 0);


SIGNAL ReleaseFlag : std_logic;


SIGNAL Column : std_logic_vector(11 DOWNTO 0);
SIGNAL Row : std_logic_vector(11 DOWNTO 0);
SIGNAL Newframe_Send_Flag : std_logic;


signal starsInFrame : std_logic_vector(4 DOWNTO 0);
CONSTANT MaxStarsInFrame : std_logic_vector(7 DOWNTO 0) := x"19";

---Divider1
SIGNAL a_div1 : std_logic_vector(23 DOWNTO 0);
SIGNAL b_div1 : std_logic_vector(23 DOWNTO 0);
SIGNAL result_div1 : std_logic_vector(23 DOWNTO 0);
SIGNAL buffer_div1 : std_logic_vector(47 DOWNTO 0);
SIGNAL cnt_div1 : std_logic_vector(4 DOWNTO 0);


---Divider2
SIGNAL a_div2 : std_logic_vector(23 DOWNTO 0);
SIGNAL b_div2 : std_logic_vector(23 DOWNTO 0);
SIGNAL result_div2 : std_logic_vector(23 DOWNTO 0);
SIGNAL buffer_div2 : std_logic_vector(47 DOWNTO 0);

---Divider3
SIGNAL a_div3 : std_logic_vector(23 DOWNTO 0);
SIGNAL b_div3 : std_logic_vector(23 DOWNTO 0);
SIGNAL result_div3 : std_logic_vector(23 DOWNTO 0);
SIGNAL buffer_div3 : std_logic_vector(47 DOWNTO 0);

SIGNAL cnt_div23 : std_logic_vector(4 DOWNTO 0);

--
---Nowthejourneybegins!
begin
	ThresholdUpdate : process(RESET,CLK)
	BEGIN
		IF(RESET = '1')THEN
			Threshold_refresh_old <= '0';
			Threshold_Value <= x"200";--Defaultvalue,512
		elsif falling_edge(CLK) THEN
			--RefreshthresholdvalueonrisingedgeofthethresholdrefreshSIGNAL
			if Threshold_refresh = '1' and Threshold_refresh_old = '0' then
				Threshold_Value <= Threshold_in(11 DOWNTO 0);
			end if;
			Threshold_refresh_old <= Threshold_refresh;
		end if;
	END PROCESS;
	Blobbing : process(CLK,Reset) --Blobdetectionalgorithm
	begin
		if Reset /= '0'then
			Blobber_State <= x"0";
			XI_sum <= x"000000";
			I_sum <= x"000000";
			Column <= x"000";
			Row <= x"000";
			Blob_Row <= x"000";
			RowBlob_Start_Column <= x"000";
			RowBlob_Start_Column_New <= x"000";
			Data_Prepare_State <= x"0";
			New_Blob_To_Calculate <= '0';
			FIFO_we <= '0';
			FIFO_re <= '0';
			FIFO1_Conditioner_State <= x"0";
			FIFO1_Data_Snatched <= '0';
			New_Blob_Detected <= '0';

			--Importanttohavethishere!.Otherwiseifitis1atstartthestatemachinewillgetstuck
			FIFO1_Data_Snatched <= '0';
			RowBlob_NewFlag <= '0';
			New_Frame <= '0';

			--Fourthstatemachineforreleasingblobs
			Release_State <= x"0";
			ReleaseFlag <= '0';
			Ghosting <= x"00";
			StarsInFrame <= "00000";
		elsif falling_edge(CLK) then
			--Outsidetheframe.Clearvariables
			if FrameValid = '0'then
				Column <= x"000";
				Row <= x"000";
				Blobber_State <= x"0";
				StarsInFrame <= "00000";
				
				--Cleartheblobcounterforeachnewframe
			else
				Newframe_Send_Flag <= '1';
			end if;

		--Theghostinghelpsfordeadpixelsinarowanddecideswhena-blobonrowisended
			if Ghosting > 0 then
			Ghosting <= Ghosting - 1;
			end if;


			--
			--Algorithmlayer4
			case Release_State is
				when x"0" => 
				--Idlestate,waitforsomedata
				if ReleaseFlag = '1'then
					Tmp2_message1 <= Tmp1_message1;

					--PutdatatothetemporarySIGNAL vector
					Tmp2_message2 <= Tmp1_message2;

					--PutdatatothetemporarySIGNAL vector
					Release_State <= x"1";
					New_Frame <= '0';

					--Ordinarymessage
				elsif FrameValid = '0' and Newframe_Send_Flag = '1' then
					--Outsidetheframe,sendonenewframemessage
					Release_State <= x"D";
					Newframe_Send_Flag <= '0';
					New_Frame <= '1';

					--ThistellstheBlobmessagethatIshouldsendaNewFrameMessage
				end if;

				when x"1" =>
					Release_State <= x"2";

					--Divider2calculatestheXcenterbymean
					a_div2 <= Tmp2_message1(63 DOWNTO 40);

					b_div2 <= "000000000000000000" & Tmp2_message1(15 DOWNTO 10);

					a_div3 <= Tmp2_message2(63 DOWNTO 40);

					b_div3 <= Tmp2_message2(39 DOWNTO 16);

				when x"2" =>
					Release_State <= x"3";
					buffer_div2 <= x"000000" & a_div2;

					buffer_div3 <= x"000000" & a_div3;

					cnt_div23 <= "00000";

				when x"3" => 
			--Divideloopstate
			--Divider2
					if buffer_div2((48-2) DOWNTO 23) >= b_div2 then
						buffer_div2(47 DOWNTO 24) <= '0' & (buffer_div2((48-3) DOWNTO 23) - b_div2((24-2) DOWNTO 0));
						buffer_div2(23 DOWNTO 0) <= buffer_div2(22 DOWNTO 0) & '1';
					else
						buffer_div2 <= buffer_div2(46 DOWNTO 0) & '0';
					end if;

			--Divider3
					if buffer_div3((48-2) DOWNTO 23) >= b_div3 then
						buffer_div3(47 DOWNTO 24) <= '0' & (buffer_div3((48-3) DOWNTO 23) - b_div3((24-2) DOWNTO 0));
						buffer_div3(23 DOWNTO 0) <= buffer_div3(22 DOWNTO 0) & '1';
					else
						buffer_div3 <= buffer_div3(46 DOWNTO 0) & '0';
					end if;
					if cnt_div23 /= "11000" then --24cycles
						cnt_div23 <= cnt_div23 + 1;
						Release_State <= x"3";
					else
						result_div2 <= buffer_div2(23 DOWNTO 0);
						result_div3 <= buffer_div3(23 DOWNTO 0);
						Release_State <= x"4";
					end if;

				when x"4" => --Divisionisdone
					Release_State <= x"D";

				when x"D" =>
					Release_State <= x"E";

					if ((Tmp2_message1(9 DOWNTO 0) > 1) or New_Frame = '1') then --Therewheremorethan1pixelsintheblob
						New_Blob_Detected <= '1';

			--EnablewritingtoUARTbuffer

						if New_Frame = '1' then --Newframemessage
							Blob_Data_Out <= "1" & "000" & "0000" & x"000000";
						else --ordinarymessage

							Blob_Data_Out <= "0" & "000" & "0000" & result_div2(11 DOWNTO 0) & ((Tmp2_message1(27 DOWNTO 16)-("000000" & Tmp2_message1(15 DOWNTO 10))) + result_div3(11 DOWNTO 0));
							
							--Increasethedetectedstarcounter
							StarsInFrame <= StarsInFrame + 1;
						end if;
					end if;
				when x"E" =>
					Release_State <= x"F";
					if New_Frame = '1'then --Newframemessage
						Blob_Data_Out <= x"00000000"; --Thesecondmessage
					else
						Blob_Data_Out <= Tmp2_message2(39 DOWNTO 16) & Tmp2_message1(7 DOWNTO 0);
					end if;

				when x"F" =>
					Release_State <= x"0";
					New_Blob_Detected <= '0'; --StopwritingtoUARTbuffer
					ReleaseFlag <= '0'; --Resettheflagthatstartedthisstatemachine

				when others =>
					Release_State <= x"0";
			end case;

			--Algorithmlayer3
			--ThisstatemachinegetsthedatafromtheFIFObufferandstoresitinthetemporaryvectors.
			--ItalsochecksiftheFIFO1blobshouldbereleasedornot.ItalsoservesforaddingnewblobstotheFIFO,
			--regardlesstheyareneworcomposedfromFIFO1data
			case FIFO1_Conditioner_State is
				when x"0" => --Idlestate,waitforsomedata
					if FIFO1_Data_Snatched = '0' and FIFO_empty = '0' then 
						FIFO1_Conditioner_State <= x"1";
						FIFO_re <= '1'; --NOTE!Dataisdelayed2clkcycles.Setrehigh2clkcyclesbeforeaccessingdata.
					elsif FIFO1_Data_Snatched = '1' then
						FIFO1_Conditioner_State <= x"5"; --Jumpdirectlytothewaitingstatetoreleasethebloborcombinewithnewrowdata
					elsif RowBlob_NewFlag = '1' then
						FIFO1_Conditioner_State <= x"A"; --Goandenqueuedata.SincetheFIFOisemptythisisacompletelynewblob
					end if;

				--GrabdatafromtheFIFO(itisactually2xpackages)
				when x"1" =>
					FIFO1_Conditioner_State <= x"2";

				when x"2" =>
					FIFO1_Conditioner_State <= x"3";
					FIFO1_Temp1 <= FIFO_in;
					FIFO_re <= '0';

				when x"3" =>
					FIFO1_Conditioner_State <= x"4";
					FIFO1_Temp2 <= FIFO_in;

					FIFO1_X_center_Sum	<= FIFO1_Temp1(63 DOWNTO 40);
					FIFO1_X_center		<= FIFO1_Temp1(39 DOWNTO 28);
					FIFO1_Y_stopp		<= FIFO1_Temp1(27 DOWNTO 16);
					FIFO1_Rowspan		<= "00" & FIFO1_Temp1(15 DOWNTO 10);
					FIFO1_PXcount		<= FIFO1_Temp1(9 DOWNTO 0);

				when x"4" =>
					FIFO1_Conditioner_State <= x"5";
					FIFO1_Data_Snatched <= '1'; --WehavesnatchedthedatafromtheFIFO1position.(dequeued)
					--Extractdatafromsecondpackage
					FIFO1_IrowN_sum <= FIFO1_Temp2(63 DOWNTO 40);
					FIFO1_Irow_sum <= FIFO1_Temp2(39 DOWNTO 16);

				--Thisisthewaitingstate.Checkiftheblobshouldbereleased.Ifweareinthisstateandthe
				--ghostingcounterisnotedtobe>0inacertainregion;theFIFO1blobbelongstotheongoingblob!
				--Newblobscanbeadded.
				--IfweareoutsidetheframeandstillhaveblobsintheFIFO,theFIFOiscleared,blobssenttoSPIbuffer.This
				--happensifthereareblobsonthelastrow.
				when x"5" =>
					if FrameValid = '0' and FIFO_empty = '0' then --WeneedtocleartheFIFO.Thishappensifthereareblobsonthebottomborder
						FIFO1_Conditioner_State <= x"E"; --SendtoSPIbuffer
					else
						if(Column>(FIFO1_X_center - x"00A")) and (Column<(FIFO1_X_center + x"00A")) and (Ghosting>0) then
							FIFO1_Conditioner_State <= x"6"; --OngoingrowblobbelongstotheFIFO1blob.Combine!

						elsif RowBlob_NewFlag = '1'then
							FIFO1_Conditioner_State <= x"A"; -- A completely new blob detected!

						elsif (Column>(FIFO1_X_center + x"00A")) and (FIFO1_Y_stopp /= Row) then
							FIFO1_Conditioner_State <= x"E"; --Releasetheblob,SendtoSPIbufferinnextstatemachine(layer4)
						end if;
					end if;

				when x"6" => --Waitfortheongoingrowblobtofinish.WewillmergesomedataandthenputitbackintotheFIFO
					if RowBlob_NewFlag = '1' then
						FIFO1_Conditioner_State <= x"7";
					end if;

				when x"7" => --HerewecombinethenewrowblobdatawiththedatasavedintheFIFO
					FIFO1_Conditioner_State <= x"B";
					FIFO1_Data_Snatched <= '0'; --Thepreviouslysnatcheddataisnolongervalid.Goandresnatchinstate0.
					RowBlob_X_center_Sum <= RowBlob_X_center + FIFO1_X_center_Sum;
					RowBlob_PXcount <= RowBlob_PXcount + FIFO1_PXcount;
					RowBlob_Rowspan <= FIFO1_Rowspan + 1;
					
					--BuildthesumsusedforcalculatingY-center
					RowBlob_Irow_sum <= RowBlob_Irow_sum + FIFO1_Irow_sum;
					RowBlob_IrowN_sum <= RowBlob_IrowN_sum(23 DOWNTO 0)*FIFO1_Rowspan + FIFO1_IrowN_sum;

				when x"A" => --Wegohereifitwasacompletelynewblob
					FIFO1_Conditioner_State <= x"B";
					RowBlob_Rowspan <= x"01"; --onerow
					RowBlob_X_center_Sum <= x"000" & RowBlob_X_center; --Thefirstvaluetothesum

				--ThispartqueuesdatatotheFIFO
				when x"B" =>
					FIFO1_Conditioner_State <= x"C";

					--Clockoutthefirstmessage
					FIFO_out <= RowBlob_X_center_Sum(23 DOWNTO 0) & RowBlob_X_center(11 DOWNTO 0) & RowBlob_Row(11 DOWNTO 0) & RowBlob_Rowspan(5 DOWNTO 0) & RowBlob_PXcount(9 DOWNTO 0);
					FIFO_we <= '1';

				when x"C" =>
					FIFO1_Conditioner_State <= x"D";
					--Clockoutthesecondpacket
					FIFO_out <= RowBlob_IrowN_sum(23 DOWNTO 0) & RowBlob_Irow_sum(23 DOWNTO 0) & x"ABCD";

				when x"D" =>
					FIFO1_Conditioner_State <= x"0";
					FIFO_we <= '0';
					RowBlob_NewFlag <= '0'; --Clearthesendingflag

				when x"E" => --Createthetempmessages
					FIFO1_Conditioner_State <= x"0";
					ReleaseFlag <= '1'; --Thisstartsthefourthstatemachine
					Tmp1_message1 <= FIFO1_X_center_Sum(23 DOWNTO 0) & FIFO1_X_center(11 DOWNTO 0) & FIFO1_Y_stopp(11 DOWNTO 0) & FIFO1_Rowspan(5 DOWNTO 0) & FIFO1_PXcount(9 DOWNTO 0);
					Tmp1_message2 <= FIFO1_IrowN_sum(23 DOWNTO 0) & FIFO1_Irow_sum(23 DOWNTO 0) & x"0000";
					FIFO1_Data_Snatched <= '0'; --WehaveconsumedthedatatemporarystoredfromFIFO1.Weneedtogetsome-newdata
					RowBlob_NewFlag <= '0';

				when others =>
					FIFO1_Conditioner_State <= x"0";

			end case;


			--Algorithmlayer2
			--Preparethedataforenqueueing
			case Data_Prepare_State is
				when x"0" => --Idle,waitforsomedata
					if New_Blob_To_Calculate = '1' then
						Data_Prepare_State <= x"1";

						--Resettheflagthatbroughtushere
						New_Blob_To_Calculate <= '0';
						
						--Relevantstuffforthecenterofintensitycalculation
						a_div1 <= XI_sum_New; --numerator
						b_div1 <= I_sum_New; --denominator
						
						--Wealreadehavethisdatasojustmoveittotheirrespectivelysignals
						RowBlob_Irow_sum <= I_sum_New;
						RowBlob_IrowN_sum(31 DOWNTO 0) <= x"00" & I_sum_New; --Thisisthefirstvalueson = 1(IsumNew*n = IsumNew)
						RowBlob_Row <= Blob_Row;
						RowBlob_PXcount <= "0000" & Blob_Width;
						RowBlob_Start_Column <= RowBlob_Start_Column_New;
					end if;
				when x"1" => --Startdividing!
					if Blob_Width = x"01" then --Candivideexactlyonetimeifthereisonly1pixelintherowblob
						result_div1 <= x"000001";
						Data_Prepare_State <= x"3";
					else --Wehavetodosomecalculations...
						cnt_div1 <= "00000";
						buffer_div1 <= x"000000" & a_div1;
						Data_Prepare_State <= x"2"; --Goanddosomedivisionlooping! : )
					end if;

				when x"2" => --Divideloopstate
					if buffer_div1((48-2) DOWNTO 23) >= b_div1 then
						buffer_div1(47 DOWNTO 24) <= '0' & (buffer_div1((48-3) DOWNTO 23)-b_div1((24-2) DOWNTO 0));
						buffer_div1(23 DOWNTO 0) <= buffer_div1(22 DOWNTO 0) & '1';
					else
						buffer_div1 <= buffer_div1(46 DOWNTO 0) & '0';
					end if;

					if cnt_div1 /= "11000" then--24cycles
						cnt_div1 <= cnt_div1 + 1;
						Data_Prepare_State <= x"2";
					else
						result_div1 <= buffer_div1(23 DOWNTO 0);
						Data_Prepare_State <= x"3";
					end if;

				when x"3" => --Divisionisdone
					Data_Prepare_State <= x"0";
					RowBlob_NewFlag <= '1'; --Thistriggersthenextstatemachine
					RowBlob_X_center <= result_div1(11 DOWNTO 0) + RowBlob_Start_Column; --Xcenteristheresultfromthedivision

				when others =>
					Data_Prepare_State <= x"0";

				end case;

				--Algorithmlayer1
				--Thisstatemachinechecksifpixelvalue>threshold.Ifsoitintegratestheintensityandrelevantsums.
				--Whenwearedoneintegratingtheblobonthisrow,thepixelvalueis<thresholdandghosting = 0wesetthe
				--flagNewBlobToCalculate <= '1'.Thissignalsthatwecantriggertheotherstatemachinetostartdoingsomecalculations.
				if LineValid = '1' then
					--Countingrowsandcolumns
					if Column /= x"A1F" then --(2592-1)
						Column <= Column + 1;
					else
						Column <= x"000";
						Row <= Row + 1;
					end if;

					case Blobber_State is
						when x"0" => ---idle.Waitingforthresholdoverride
							if Data_Camera > Threshold_Value then
								Blobber_State <= x"1";
								Ghosting <= Ghosting_Level;
								--initiatevariables
								RowBlob_Start_Column_New <= Column;
								Blob_Width <= x"01";
								XI_sum <= x"000" & Data_Camera;
								I_sum <= x"000" & Data_Camera;
							end if;

						when x"1" =>
							if Data_Camera > Threshold_Value then
								Ghosting <= Ghosting_Level;
								Blob_Width <= Blob_Width + 1;
								XI_sum <= XI_sum + Blob_Width*(Data_Camera);
								I_sum <= I_sum + Data_Camera;
							end if;

							if Ghosting = x"00" then --Ifghostingis0,nextstep
								Blobber_State <= x"2";
								XI_sum_New <= XI_sum;
								I_sum_New <= I_sum;
								Blob_Row <= Row;
							end if;

						when x"2" =>
							Blobber_State <= x"0"; --Goback
							if StarsInFrame < MaxStarsInFrame then --Onlyprocessthestarifwehavedetectedlessthanthemaximumnumberofstarsinaframe
								New_Blob_To_Calculate <= '1'; --CalculatetheCenterofintensityoftheblobrow,startnextstatemachine
							end if;
						when others =>
							Blobber_State <= x"0";
					end case;
				end if;
			end if;
		end process;
end behavioral;
