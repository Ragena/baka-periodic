#include <stdio.h>      /* printf, fgets */
#include <stdlib.h>     /* atol */
#include <stdint.h>
int32_t divide(int32_t n, int32_t d)
{
        uint16_t N, D;
        if (n < 0)
                N = -n;
        else
                N = n;

        if (d < 0)
                D = -d;
        else
                D = d;

        if (!N || !D || N < D)
                return 0;

        uint16_t Q = 0;
        uint16_t R = 0;
        int8_t i;
        for (i = 15; i >= 0; i--)
        {
                uint16_t x = 1 << i;


                Q <<= 1;
                R <<= 1;

                if (N & x)
                        R = R + 1;

                if (R >= D)
                {
                        R = R - D;
                        Q = Q + 1;
                }
        }
        if ((n < 0) != (d < 0)) //An XOR of the two is true
                return -Q;
        return Q;
}
int main(int argc, char* argv[])
{
	printf("%d\n", divide(atol(argv[1]), atol(argv[2])));
}
