import pyfits
from sys import argv
from math import sqrt


class Blob:
	def __init__(self):
		self.size = 0
		self.sum = 0
		self.x_sum = 0
		self.y_sum = 0
		self.parts = [[0 for i in range(2050)] for j in range(2050)]

	def add_to_blob(self, x, y, pix):
		if pix:
			self.size += 1
			self.sum += pix
			self.x_sum += x * pix
			self.y_sum += y * pix
			self.parts[x][y] = 1

	def find_in_blob(self, x, y):
		return self.parts[x][y]

	def get(self):
		if self.size > 4:
			return (round(float(self.x_sum)/self.sum, 3), round(float(self.y_sum)/self.sum, 3), self.sum, self.size)

M = 0
oM = 0
S = 0
k = 0



def thresh(x, cutoff):
	global M
	global oM
	global S
	global k

	k += 1
	oM = M
	M = M - int((M - x) / k)
	if M < 0:
		M = 0
	S += (x - oM) * (x - M)

	if S > 0 and k > 1:
		#print(S, k, S/(k-1))
		sigma = int(sqrt(S/(k - 1)))
	else:
		sigma = 0

	if x - (M+5*sigma) <=0:
		x = 0
	else:
		x -= (M+5*sigma)
	#if abs(x - M) < cutoff * sigma:
	#	x = 0

	return x

print("Opening file " + argv[1])

img = 0

try:
	img = pyfits.getdata(argv[1], ignore_missing_end = True)

except:
	print("Bad fileargv[1].") 
	exit(-1)

print("File opened.");

pixel_buffer = [None for i in range(len(img))]

blobs = []

GLOBAL_X = 0
GLOBAL_Y = 0

Last_active_blob = None
size = 0

for rida in img:
	for pixel in range(len(rida)):
		rida[pixel] = int(thresh(rida[pixel], 5))

pyfits.writeto(argv[1]+"_sigma.fts", img, output_verify = "fix", clobber = True)