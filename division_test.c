#include <stdio.h>      /* printf, fgets */
#include <stdlib.h>     /* atol */
#include <stdint.h>

uint16_t M = 0;
uint16_t oM = 0;
uint32_t S = 0;
uint16_t k = 0;

uint16_t diff_M = 0;
uint16_t diff_oM = 0;
uint32_t diff_S = 0;
uint16_t diff_k = 0;

int32_t divide(int32_t n, int32_t d)
{
	uint16_t N, D;
	if (n < 0)
		N = -n;
	else
		N = n;

	if (d < 0)
		D = -d;
	else
		D = d;

	if (!N || !D || N < D)
		return 0;

	uint16_t Q = 0;
	uint16_t R = 0;
	int8_t i;
	for (i = 15; i >= 0; i--)
	{
		uint16_t x = 1 << i;


		Q <<= 1;
		R <<= 1;

		if (N & x)
			R = R + 1;

		if (R >= D)
		{
			R = R - D;
			Q = Q + 1;
		}
	}
	if ((n < 0) != (d < 0)) //An XOR of the two is true
		return -Q;
	return Q;
}

void mean_norm(uint16_t x)
{
	k++;
	M = M + (x - M)/k;
	
}

void mean_diff(uint16_t x)
{
	diff_k++;
	diff_M = diff_M + divide((x - diff_M), diff_k);
}

int main(int argc, char* argv[])
{
	unsigned long i;
	FILE* input_file = fopen(argv[1], "r");
	FILE* out_file = fopen(argv[2], "w");

	fprintf(out_file, "x, normal_mean, different_mean\n");
	while(!feof(input_file))
	{
		fscanf(input_file, "%lu\n", &i);
		mean_norm(i);
		mean_diff(i);
		fprintf(out_file, "%lu, %u, %u\n", i,  M, diff_M);
	}
	fclose(input_file);
	fclose(out_file);
}