#include "stdio.h"
#include "stdlib.h"
#include <stdint.h>

int j = 0;
/** This function takes two 32-bit integer numbers and divides them.
 * @arg n : The numerator
 * @arg d : The denominator
 * @ret   : The Quotient, rounded down.
 */
int32_t divide(int32_t n, int32_t d)
{
	// Use the absolute value of the numerators and denominators.
        uint16_t N, D;
        if (n < 0)
                N = -n;
        else
                N = n;

        if (d < 0)
                D = -d;
        else
                D = d;

	// If either the numerator or denominator are 0, then return 0.
        if (!N || !D || N < D)
                return 0;

	// Quotient and Remainder
        uint16_t Q = 0;
        uint16_t R = 0;
        int8_t i;
        for (i = 15; i >= 0; i--)
        {
                uint16_t x = 1 << i;


                Q <<= 1;
                R <<= 1;

                if (N & x)
                        R = R + 1;

                if (R >= D)
                {
                        R = R - D;
                        Q = Q + 1;
                }
		j++;
        }
        if ((n < 0) != (d < 0)) //An XOR of the two is true
		//If the numerator XOR the denominator is negative, return a negative Quotient.
                return -Q;
        return Q;
}


int newt_sqrt(int num) {
	int k, x = 1;
	while(1)
	{
		k = (x + divide(num, x)) >> 1;
		if(x == k || x - k == 1 || k - x == 1)
		{
			return k;
		}
		x = k;
		j ++;
	}
}

long isqrt(long num) {
    long res = 0;
    long bit = 1 << 14;
    while (bit > num)
    {
        bit >>= 2;
        j++;
    }

    while (bit != 0) {
        if (num >= res + bit) {
            num -= res + bit;
            res = (res >> 1) + bit;
        }
        else
            res >>= 1;
        bit >>= 2;
        j++;
    }
    return res;
}


int main(int argc, char* argv[])
{
	printf("%s, ", argv[1]);
	printf("%d, ", newt_sqrt(atoi(argv[1])));
	printf("%d, ", j);
	j = 0;
	printf("%lu, ", isqrt(atoi(argv[1])));
	printf("%d\n", j);

}
