
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

package arith is

	function bin_sqrt ( num : in UNSIGNED) return UNSIGNED;
	function divide ( NUMERATOR,DENOMINATOR : in UNSIGNED) return UNSIGNED;

end arith;

package body arith is

	function bin_sqrt (num : in UNSIGNED ) return UNSIGNED is
		variable int_num : UNSIGNED (num'Length-1 downto 0);
		variable res : UNSIGNED (num'Length-1 downto 0) := (others => '0');
		variable bitt : UNSIGNED (num'Length-1 downto 0) := (0 => '1', others => '0');
	begin
		bitt := shift_left(bitt, bitt'Length-2);
		int_num := num;
		if (num < 0) then
			int_num := not int_num;
			int_num := int_num - 1;
		end if;
		L1: for i in 0 to 31 loop
			exit L1 when bitt <= num;
			bitt := shift_right(bitt, 2);
		end loop L1;
		
		L2: for i in 0 to num'Length-1 loop
			exit L2 when bitt = 0;
			if (int_num >= (res + bitt)) then
				int_num := int_num - (res + bitt);
				res := shift_right(res, 1);
				res := res + bitt;
			else
				res := shift_right(res, 1);
			end if;
			
			bitt := shift_right(bitt, 2);
		end loop L2;
		
		return res;
		
	end bin_sqrt;
		

		function divide ( NUMERATOR,DENOMINATOR : in UNSIGNED) return UNSIGNED is
			variable N : UNSIGNED (NUMERATOR'Length-1 downto 0) := (others => '0');
			variable D : UNSIGNED (DENOMINATOR'Length-1 downto 0) := (others => '0');
			variable Q : UNSIGNED (NUMERATOR'Length-1 downto 0) := (others => '0');
			variable R : UNSIGNED (DENOMINATOR'Length-1 downto 0) := (others => '0');
		begin
			N := NUMERATOR;
			
			if (N = 0 or D = 0 or N < D) then
				return Q;
			end if;
			
			L: for i in 15 downto 0 loop
			
				Q := shift_left(Q, 1);
				R := shift_left(R, 1);
				
				R(0) := N(i);
				
				if (R >= D) then
					R := R - D;
					Q := Q + 1;
				end if;
				
			end loop L;
			
			if (NUMERATOR < 0 xor DENOMINATOR < 0) then
				Q := not Q;
				Q := Q + 1;
			end if;
			
			return Q;
			
		end divide;
		
end arith;
