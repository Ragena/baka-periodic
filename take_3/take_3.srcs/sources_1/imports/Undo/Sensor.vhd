library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

package Sensor is

    constant LENGTH_X : INTEGER := 2592;
    constant LENGTH_Y : INTEGER := 1944;
    constant N_X : INTEGER := 11; -- This is the number of bits in the number of pixels in the sensor: log(LENGTH_X)(log(2)
    constant N_Y : INTEGER := 10; -- This is the number of bits in the number of pixels in the sensor: log(LENGTH_Y)(log(2)
    constant N : INTEGER := 22; -- This is the number of bits in the number of pixels in the sensor: log(LENGTH_X*LENGTH_Y)(log(2)
    constant MAX_BLOBS : INTEGER := 10;

    type bit_map is array (0 to 9) of STD_LOGIC_VECTOR (19 downto 0);

    type blob is record
        first_x : UNSIGNED (n_x downto 0);
        first_y : UNSIGNED (n_y downto 0);
        b_map : bit_map;
        x_sum : UNSIGNED (15 downto 0);
        y_sum : UNSIGNED (15 downto 0);
        sum : UNSIGNED (15 downto 0);
        n : INTEGER range 0 to 127;
    end record;

    type blob_array is array (0 to MAX_BLOBS-1) of blob;
    type pixel_array is array (0 to LENGTH_X-1) of UNSIGNED (15 downto 0);

end Sensor;

package body Sensor is

end Sensor;
