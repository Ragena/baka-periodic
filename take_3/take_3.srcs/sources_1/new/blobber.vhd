----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/25/2016 10:49:48 PM
-- Design Name: 
-- Module Name: blobber - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity blobber is
    Port ( pixel_in : in STD_LOGIC_VECTOR (15 downto 0);
           clk : in STD_LOGIC;
           blobs : out STD_LOGIC);
end blobber;

architecture Behavioral of blobber is

    component thresholding is
        Port ( pixel_in : in UNSIGNED (15 downto 0);
               pixel_out : out UNSIGNED (15 downto 0);
               new_pixel : STD_LOGIC;
               clk : in STD_LOGIC);
    end component;

    signal pixel_threshed : UNSIGNED (15 downto 0);
    signal new_pixel : STD_LOGIC;
    signal x_coord : integer range 0 to LENGTH_X - 1 := 0;
    signal y_coord : integer range 0 to LENGTH_Y - 1 := 0;
    signal b_array_top : integer range 0 to MAX_BLOBS - 1 := 0;
    signal active_blob : integer range 0 to MAX_BLOBS - 1 := 0;

    signal b_array : blob_array;
    signal p_array : pixel_array;
begin

    thresh : thresholding PORT MAP(
        pixel_in => pixel_in,
        pixel_out => pixel_threshed,
        new_pixel => new_pixel,
        clk => clk);

    coords : process (new_pixel)
    begin
        if x_coord = LENGTH_X - 1 then
            x_coord <= 0;
            if y_coord = LENGTH_Y - 1 then
                y_coord <= 0;
            else
                y_coord <= y_coord + 1;
            end if;
        else
                x_coord <= x_coord + 1;
        end if;
    end process;
    
    search_for_blobs : process(x_coord)
        variable last_active_blob : integer range 0 to MAX_BLOBS - 1 := 0;
        variable x_crd : integer range 0 to LENGTH_X - 1 := 0;
        variable y_crd : integer range 0 to LENGTH_Y - 1 := 0;
    begin
        if x_coord > b_array(last_active_blob).first_x then
            x_crd := x_coord - b_array(last_active_blob).first_x;
        else
            x_crd := b_array(last_active_blob).first_x - x_coord;
        end if;

        y_crd := y_coord - b_array(last_active_blob).first_y;
        
        FIND_BLOB : for i in 0 to b_array_top loop
            
        end loop FIND_BLOB;
    end process;

    proc : process (new_pixel)
    begin
        img_done <= '0';
        if pixel = X"0" then -- This pixel didn't pass the threshold.
            if blobless > 0 then
                if last_active_blob = -1 then -- There is no "last active blob"
                    last_active_blob <= b_array_top; -- Assigning a new blob value as the "last active blob"
                    b_array_top <= b_array_top + 1; -- Increasing the array head.
                end if;
                
                ADD_BLOBLESS_1: for i in 1 to 20 loop -- Adding each blobless pixel to the new blob.
                    if i > blobless then
                        exit ADD_BLOBLESS_1;
                    end if;
                    b_array(last_active_blob).b_map(y_index)(x_index - i) <= '1';
                    b_array(last_active_blob).x_sum <= b_array(last_active_blob).x_sum + ((x_index - i) * p_array(x_index - i));
                    b_array(last_active_blob).y_sum <= b_array(last_active_blob).y_sum + (y_index * p_array(x_index - i));
                    b_array(last_active_blob).sum <= b_array(last_active_blob).sum + p_array(x_index - i);
                    b_array(last_active_blob).n <= b_array(last_active_blob).n + 1;
                end loop ADD_BLOBLESS_1;
                blobless <= 0;
            else
                last_active_blob <= -1;                    
            end if;
        else -- This pixel is not equal to zero, therefore passed the threshold.
            if last_active_blob = -1 then -- There is no "last active blob"
                FIND_BLOB: for i in 0 to b_array_top-1 loop -- Search through the blob array, to find a blob this pixel can join.
                    if b_array(i).b_map(y_index - 1)(x_index) = '1' then
                        last_active_blob <= i;
                        exit FIND_BLOB;
                    end if;
                end loop FIND_BLOB;
                
                if last_active_blob = -1 then -- The loop didn't find anything
                    blobless <= blobless + 1;
                else
                    -- Add this pixel to the blob
                    b_array(last_active_blob).b_map(y_index)(x_index) <= '1';
                    b_array(last_active_blob).x_sum <= b_array(last_active_blob).x_sum + (x_index * pixel);
                    b_array(last_active_blob).y_sum <= b_array(last_active_blob).y_sum + (y_index * pixel);
                    b_array(last_active_blob).sum <= b_array(last_active_blob).sum + (pixel);
                    b_array(last_active_blob).n <= b_array(last_active_blob).n + 1;
                        
                    ADD_BLOBLESS_2: for i in 1 to 20 loop -- Adding each blobless pixel to the new blob.
                        if i > blobless then
                            exit ADD_BLOBLESS_2;
                        end if;
                        b_array(last_active_blob).b_map(y_index)(x_index - i) <= '1';
                        b_array(last_active_blob).x_sum <= b_array(last_active_blob).x_sum + ((x_index - i) * p_array(x_index - i));
                        b_array(last_active_blob).y_sum <= b_array(last_active_blob).y_sum + (y_index * p_array(x_index - i));
                        b_array(last_active_blob).sum <= b_array(last_active_blob).sum + p_array(x_index - i);
                        b_array(last_active_blob).n <= b_array(last_active_blob).n + 1;
                    end loop ADD_BLOBLESS_2;
                    blobless <= 0;
                end if;
            end if;                
        end if;
        p_array(x_index) <= pixel;
            
        x_index <= x_index + 1;
        if x_index = 0 then
            y_index <= y_index + 1;
            if y_index = 0 then
                img_done <= '1';
                -- TODO: Reinitialize signals somewhere here.
            end if;
        end if;
    end process;

end Behavioral;
