----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/25/2016 07:48:47 PM
-- Design Name: 
-- Module Name: thresholding - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use WORK.ARITH.ALL;
use WORK.SENSOR.ALL;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity thresholding is
    Port ( pixel_in : in UNSIGNED (15 downto 0);
           pixel_out : out UNSIGNED (15 downto 0);
           new_pixel : STD_LOGIC;
           clk : in STD_LOGIC);
end thresholding;

architecture Behavioral of thresholding is

    signal M : UNSIGNED (15 downto 0) := (others => '0'); -- the current arithmetic mean
    signal oM : UNSIGNED (15 downto 0) := (others => '0'); -- the old arithmetic mean
    signal k : INTEGER range 0 to LENGTH_X*LENGTH_Y := 0; -- the total number of pixels obtained
    signal sigma : UNSIGNED (16 downto 0) := (others => '0');

begin
    
    averaging : process(pixel_in)
    -- this process calculates a new average with every change in "pixel_in"
        variable dif : UNSIGNED (15 downto 0) := (others => '0');        
    begin
        k <= k + 1;
        
        if M > pixel_in then
            dif := M - pixel_in;
            M <= M + divide(dif, to_unsigned(k, N));
        else
            dif := pixel_in - M;
            M <= M - divide(dif, to_unsigned(k, N));
        end if;
    end process;
    
    std_dev : process (M)
    --this process calculates the standard deviation with every new average
        variable dif : UNSIGNED (15 downto 0) := (others => '0');
        variable odif : UNSIGNED (15 downto 0) := (others => '0');
        variable active_pixel : UNSIGNED (15 downto 0) := (others => '0');
        variable S : UNSIGNED (32 downto 0) := (others => '0');
    begin
        active_pixel := pixel_in;
        
        if M > active_pixel then
            dif := M - active_pixel;
        else
            dif := active_pixel - M;
        end if;
        
        if oM > active_pixel then
            odif := oM - active_pixel;
        else
            odif := active_pixel - oM;
        end if;
        
        S := divide (S + dif*odif, to_unsigned(k-1, N));
        sigma <= bin_sqrt(S)(16 downto 0);                
    end process;
    
    threshy : process(sigma)
    --this process calculates the new value of the pixel, based on the threshold.
        variable f_sigma : UNSIGNED (16 downto 0) := (others => '0');
    begin
        f_sigma := shift_left(sigma, 2) + sigma; -- multiply sigma by 5: shift left twice, then add sigma.
        if pixel_in < M + f_sigma then
            pixel_out <= (others => '0');
        else
            pixel_out <= pixel_in - (M + f_sigma(15 downto 0));
        end if;
        new_pixel <= !new_pixel;
    end process;

end Behavioral;
