----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/25/2016 11:04:41 PM
-- Design Name: 
-- Module Name: thresholding_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity thresholding_sim is
end thresholding_sim;

architecture Behavioral of thresholding_sim is

    component thresholding is
        Port ( pixel_in : in UNSIGNED (15 downto 0);
               pixel_out : out UNSIGNED (15 downto 0);
               clk : in STD_LOGIC);
    end component;
    
    signal in_pixel : UNSIGNED (15 downto 0) := (others => '0');
    signal out_pixel : UNSIGNED (15 downto 0) := (others => '0');
    
begin

    thresh : thresholding PORT MAP (
        pixel_in => pixel,
        pixel_out => output,
        clk => clk);

      --  Test Bench Statements
     tb : PROCESS
     BEGIN
			L: while sqr < 65535 loop
			
			root <= sqrt(sqr);
			
			wait for 100 ns; -- wait until global set/reset completes
			end loop L;
		  

            -- Add user defined stimulus here
            in_pixel <= X"2310";
            wait for 100 ns;
            in_pixel <= X"2310";
            wait for 100 ns;
            in_pixel <= X"0210";
            wait for 100 ns;
            in_pixel <= X"0310";
            wait for 100 ns;
            in_pixel <= X"2310";
            wait for 100 ns;
            in_pixel <= X"F310";
            wait for 100 ns;
            in_pixel <= X"4310";
            wait for 100 ns;
            in_pixel <= X"3310";
            wait for 100 ns;
            in_pixel <= X"D310";
            wait for 100 ns;
            in_pixel <= X"0310";
            wait for 100 ns;
            

            wait; -- will wait forever
     END PROCESS tb;
  --  End Test Bench 

end Behavioral;
