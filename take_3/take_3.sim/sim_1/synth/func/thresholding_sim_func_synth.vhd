-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.1 (lin64) Build 1538259 Fri Apr  8 15:45:23 MDT 2016
-- Date        : Wed May 25 23:16:26 2016
-- Host        : Nein running 64-bit unknown
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               /home/andu/baka/take_3/take_3.sim/sim_1/synth/func/thresholding_sim_func_synth.vhd
-- Design      : thresholding
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a35tcpg236-3
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity thresholding is
  port (
    pixel_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    pixel_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clk : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of thresholding : entity is true;
end thresholding;

architecture STRUCTURE of thresholding is
  signal pixel_in_IBUF : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pixel_out_OBUF : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \pixel_out_OBUF[11]_inst_i_1_n_0\ : STD_LOGIC;
  signal \pixel_out_OBUF[11]_inst_i_1_n_1\ : STD_LOGIC;
  signal \pixel_out_OBUF[11]_inst_i_1_n_2\ : STD_LOGIC;
  signal \pixel_out_OBUF[11]_inst_i_1_n_3\ : STD_LOGIC;
  signal \pixel_out_OBUF[11]_inst_i_2_n_0\ : STD_LOGIC;
  signal \pixel_out_OBUF[11]_inst_i_3_n_0\ : STD_LOGIC;
  signal \pixel_out_OBUF[11]_inst_i_4_n_0\ : STD_LOGIC;
  signal \pixel_out_OBUF[11]_inst_i_5_n_0\ : STD_LOGIC;
  signal \pixel_out_OBUF[15]_inst_i_1_n_1\ : STD_LOGIC;
  signal \pixel_out_OBUF[15]_inst_i_1_n_2\ : STD_LOGIC;
  signal \pixel_out_OBUF[15]_inst_i_1_n_3\ : STD_LOGIC;
  signal \pixel_out_OBUF[15]_inst_i_2_n_0\ : STD_LOGIC;
  signal \pixel_out_OBUF[15]_inst_i_3_n_0\ : STD_LOGIC;
  signal \pixel_out_OBUF[15]_inst_i_4_n_0\ : STD_LOGIC;
  signal \pixel_out_OBUF[15]_inst_i_5_n_0\ : STD_LOGIC;
  signal \pixel_out_OBUF[3]_inst_i_1_n_0\ : STD_LOGIC;
  signal \pixel_out_OBUF[3]_inst_i_1_n_1\ : STD_LOGIC;
  signal \pixel_out_OBUF[3]_inst_i_1_n_2\ : STD_LOGIC;
  signal \pixel_out_OBUF[3]_inst_i_1_n_3\ : STD_LOGIC;
  signal \pixel_out_OBUF[3]_inst_i_2_n_0\ : STD_LOGIC;
  signal \pixel_out_OBUF[3]_inst_i_3_n_0\ : STD_LOGIC;
  signal \pixel_out_OBUF[3]_inst_i_4_n_0\ : STD_LOGIC;
  signal \pixel_out_OBUF[3]_inst_i_5_n_0\ : STD_LOGIC;
  signal \pixel_out_OBUF[7]_inst_i_1_n_0\ : STD_LOGIC;
  signal \pixel_out_OBUF[7]_inst_i_1_n_1\ : STD_LOGIC;
  signal \pixel_out_OBUF[7]_inst_i_1_n_2\ : STD_LOGIC;
  signal \pixel_out_OBUF[7]_inst_i_1_n_3\ : STD_LOGIC;
  signal \pixel_out_OBUF[7]_inst_i_2_n_0\ : STD_LOGIC;
  signal \pixel_out_OBUF[7]_inst_i_3_n_0\ : STD_LOGIC;
  signal \pixel_out_OBUF[7]_inst_i_4_n_0\ : STD_LOGIC;
  signal \pixel_out_OBUF[7]_inst_i_5_n_0\ : STD_LOGIC;
  signal \NLW_pixel_out_OBUF[15]_inst_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
begin
\pixel_in_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => pixel_in(0),
      O => pixel_in_IBUF(0)
    );
\pixel_in_IBUF[10]_inst\: unisim.vcomponents.IBUF
     port map (
      I => pixel_in(10),
      O => pixel_in_IBUF(10)
    );
\pixel_in_IBUF[11]_inst\: unisim.vcomponents.IBUF
     port map (
      I => pixel_in(11),
      O => pixel_in_IBUF(11)
    );
\pixel_in_IBUF[12]_inst\: unisim.vcomponents.IBUF
     port map (
      I => pixel_in(12),
      O => pixel_in_IBUF(12)
    );
\pixel_in_IBUF[13]_inst\: unisim.vcomponents.IBUF
     port map (
      I => pixel_in(13),
      O => pixel_in_IBUF(13)
    );
\pixel_in_IBUF[14]_inst\: unisim.vcomponents.IBUF
     port map (
      I => pixel_in(14),
      O => pixel_in_IBUF(14)
    );
\pixel_in_IBUF[15]_inst\: unisim.vcomponents.IBUF
     port map (
      I => pixel_in(15),
      O => pixel_in_IBUF(15)
    );
\pixel_in_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => pixel_in(1),
      O => pixel_in_IBUF(1)
    );
\pixel_in_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => pixel_in(2),
      O => pixel_in_IBUF(2)
    );
\pixel_in_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => pixel_in(3),
      O => pixel_in_IBUF(3)
    );
\pixel_in_IBUF[4]_inst\: unisim.vcomponents.IBUF
     port map (
      I => pixel_in(4),
      O => pixel_in_IBUF(4)
    );
\pixel_in_IBUF[5]_inst\: unisim.vcomponents.IBUF
     port map (
      I => pixel_in(5),
      O => pixel_in_IBUF(5)
    );
\pixel_in_IBUF[6]_inst\: unisim.vcomponents.IBUF
     port map (
      I => pixel_in(6),
      O => pixel_in_IBUF(6)
    );
\pixel_in_IBUF[7]_inst\: unisim.vcomponents.IBUF
     port map (
      I => pixel_in(7),
      O => pixel_in_IBUF(7)
    );
\pixel_in_IBUF[8]_inst\: unisim.vcomponents.IBUF
     port map (
      I => pixel_in(8),
      O => pixel_in_IBUF(8)
    );
\pixel_in_IBUF[9]_inst\: unisim.vcomponents.IBUF
     port map (
      I => pixel_in(9),
      O => pixel_in_IBUF(9)
    );
\pixel_out_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => pixel_out_OBUF(0),
      O => pixel_out(0)
    );
\pixel_out_OBUF[10]_inst\: unisim.vcomponents.OBUF
     port map (
      I => pixel_out_OBUF(10),
      O => pixel_out(10)
    );
\pixel_out_OBUF[11]_inst\: unisim.vcomponents.OBUF
     port map (
      I => pixel_out_OBUF(11),
      O => pixel_out(11)
    );
\pixel_out_OBUF[11]_inst_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \pixel_out_OBUF[7]_inst_i_1_n_0\,
      CO(3) => \pixel_out_OBUF[11]_inst_i_1_n_0\,
      CO(2) => \pixel_out_OBUF[11]_inst_i_1_n_1\,
      CO(1) => \pixel_out_OBUF[11]_inst_i_1_n_2\,
      CO(0) => \pixel_out_OBUF[11]_inst_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => pixel_in_IBUF(11 downto 8),
      O(3 downto 0) => pixel_out_OBUF(11 downto 8),
      S(3) => \pixel_out_OBUF[11]_inst_i_2_n_0\,
      S(2) => \pixel_out_OBUF[11]_inst_i_3_n_0\,
      S(1) => \pixel_out_OBUF[11]_inst_i_4_n_0\,
      S(0) => \pixel_out_OBUF[11]_inst_i_5_n_0\
    );
\pixel_out_OBUF[11]_inst_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pixel_in_IBUF(11),
      O => \pixel_out_OBUF[11]_inst_i_2_n_0\
    );
\pixel_out_OBUF[11]_inst_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pixel_in_IBUF(10),
      O => \pixel_out_OBUF[11]_inst_i_3_n_0\
    );
\pixel_out_OBUF[11]_inst_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pixel_in_IBUF(9),
      O => \pixel_out_OBUF[11]_inst_i_4_n_0\
    );
\pixel_out_OBUF[11]_inst_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pixel_in_IBUF(8),
      O => \pixel_out_OBUF[11]_inst_i_5_n_0\
    );
\pixel_out_OBUF[12]_inst\: unisim.vcomponents.OBUF
     port map (
      I => pixel_out_OBUF(12),
      O => pixel_out(12)
    );
\pixel_out_OBUF[13]_inst\: unisim.vcomponents.OBUF
     port map (
      I => pixel_out_OBUF(13),
      O => pixel_out(13)
    );
\pixel_out_OBUF[14]_inst\: unisim.vcomponents.OBUF
     port map (
      I => pixel_out_OBUF(14),
      O => pixel_out(14)
    );
\pixel_out_OBUF[15]_inst\: unisim.vcomponents.OBUF
     port map (
      I => pixel_out_OBUF(15),
      O => pixel_out(15)
    );
\pixel_out_OBUF[15]_inst_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \pixel_out_OBUF[11]_inst_i_1_n_0\,
      CO(3) => \NLW_pixel_out_OBUF[15]_inst_i_1_CO_UNCONNECTED\(3),
      CO(2) => \pixel_out_OBUF[15]_inst_i_1_n_1\,
      CO(1) => \pixel_out_OBUF[15]_inst_i_1_n_2\,
      CO(0) => \pixel_out_OBUF[15]_inst_i_1_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => pixel_in_IBUF(14 downto 12),
      O(3 downto 0) => pixel_out_OBUF(15 downto 12),
      S(3) => \pixel_out_OBUF[15]_inst_i_2_n_0\,
      S(2) => \pixel_out_OBUF[15]_inst_i_3_n_0\,
      S(1) => \pixel_out_OBUF[15]_inst_i_4_n_0\,
      S(0) => \pixel_out_OBUF[15]_inst_i_5_n_0\
    );
\pixel_out_OBUF[15]_inst_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pixel_in_IBUF(15),
      O => \pixel_out_OBUF[15]_inst_i_2_n_0\
    );
\pixel_out_OBUF[15]_inst_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pixel_in_IBUF(14),
      O => \pixel_out_OBUF[15]_inst_i_3_n_0\
    );
\pixel_out_OBUF[15]_inst_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pixel_in_IBUF(13),
      O => \pixel_out_OBUF[15]_inst_i_4_n_0\
    );
\pixel_out_OBUF[15]_inst_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pixel_in_IBUF(12),
      O => \pixel_out_OBUF[15]_inst_i_5_n_0\
    );
\pixel_out_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => pixel_out_OBUF(1),
      O => pixel_out(1)
    );
\pixel_out_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => pixel_out_OBUF(2),
      O => pixel_out(2)
    );
\pixel_out_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => pixel_out_OBUF(3),
      O => pixel_out(3)
    );
\pixel_out_OBUF[3]_inst_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \pixel_out_OBUF[3]_inst_i_1_n_0\,
      CO(2) => \pixel_out_OBUF[3]_inst_i_1_n_1\,
      CO(1) => \pixel_out_OBUF[3]_inst_i_1_n_2\,
      CO(0) => \pixel_out_OBUF[3]_inst_i_1_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => pixel_in_IBUF(3 downto 0),
      O(3 downto 0) => pixel_out_OBUF(3 downto 0),
      S(3) => \pixel_out_OBUF[3]_inst_i_2_n_0\,
      S(2) => \pixel_out_OBUF[3]_inst_i_3_n_0\,
      S(1) => \pixel_out_OBUF[3]_inst_i_4_n_0\,
      S(0) => \pixel_out_OBUF[3]_inst_i_5_n_0\
    );
\pixel_out_OBUF[3]_inst_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pixel_in_IBUF(3),
      O => \pixel_out_OBUF[3]_inst_i_2_n_0\
    );
\pixel_out_OBUF[3]_inst_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pixel_in_IBUF(2),
      O => \pixel_out_OBUF[3]_inst_i_3_n_0\
    );
\pixel_out_OBUF[3]_inst_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pixel_in_IBUF(1),
      O => \pixel_out_OBUF[3]_inst_i_4_n_0\
    );
\pixel_out_OBUF[3]_inst_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pixel_in_IBUF(0),
      O => \pixel_out_OBUF[3]_inst_i_5_n_0\
    );
\pixel_out_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => pixel_out_OBUF(4),
      O => pixel_out(4)
    );
\pixel_out_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => pixel_out_OBUF(5),
      O => pixel_out(5)
    );
\pixel_out_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => pixel_out_OBUF(6),
      O => pixel_out(6)
    );
\pixel_out_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => pixel_out_OBUF(7),
      O => pixel_out(7)
    );
\pixel_out_OBUF[7]_inst_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \pixel_out_OBUF[3]_inst_i_1_n_0\,
      CO(3) => \pixel_out_OBUF[7]_inst_i_1_n_0\,
      CO(2) => \pixel_out_OBUF[7]_inst_i_1_n_1\,
      CO(1) => \pixel_out_OBUF[7]_inst_i_1_n_2\,
      CO(0) => \pixel_out_OBUF[7]_inst_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => pixel_in_IBUF(7 downto 4),
      O(3 downto 0) => pixel_out_OBUF(7 downto 4),
      S(3) => \pixel_out_OBUF[7]_inst_i_2_n_0\,
      S(2) => \pixel_out_OBUF[7]_inst_i_3_n_0\,
      S(1) => \pixel_out_OBUF[7]_inst_i_4_n_0\,
      S(0) => \pixel_out_OBUF[7]_inst_i_5_n_0\
    );
\pixel_out_OBUF[7]_inst_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pixel_in_IBUF(7),
      O => \pixel_out_OBUF[7]_inst_i_2_n_0\
    );
\pixel_out_OBUF[7]_inst_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pixel_in_IBUF(6),
      O => \pixel_out_OBUF[7]_inst_i_3_n_0\
    );
\pixel_out_OBUF[7]_inst_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pixel_in_IBUF(5),
      O => \pixel_out_OBUF[7]_inst_i_4_n_0\
    );
\pixel_out_OBUF[7]_inst_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pixel_in_IBUF(4),
      O => \pixel_out_OBUF[7]_inst_i_5_n_0\
    );
\pixel_out_OBUF[8]_inst\: unisim.vcomponents.OBUF
     port map (
      I => pixel_out_OBUF(8),
      O => pixel_out(8)
    );
\pixel_out_OBUF[9]_inst\: unisim.vcomponents.OBUF
     port map (
      I => pixel_out_OBUF(9),
      O => pixel_out(9)
    );
end STRUCTURE;
