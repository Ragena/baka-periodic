import pyfits
import sys
from myhdl import intbv

Class Pixel:
	def __init__(self, x, y, intensity):
		self.x = intbv(x, min=0)[16:]
		self.y = intbv(y, min=0)[16:]
		self.intensity = intbv(intensity, min=0)[16:]

Class Blob:
	def __init__(self):
		self.sumx = intbv(0,min=0, max=0xFFFF00) # 16bit pixels, blobs are not larger than 4x4
		self.sumy = intbv(0,min=0, max=0xFFFF00) # 16bit pixels, blobs are not larger than 4x4
		self.sum = intbv(0,min=0, max=0xFFFF*36*6) # 16bit pixels, blobs are not larger than 4x4
		self.last_y = intbv(0,min=0)[16:]
		self.x_start = intbv(0,min=0)[16:]
		self.x_stop = intbv(0,min=0)[16:]
		self.size = intbv(0,min=0, max = 36)
		self.initial_x = intbv(0,min=0)[16:]
		self.initial_y = intbv(0,min=0)[16:]

	def add_pix(self, pix):

		#First add. Boundaries are wrong.
		if self.size == 0:
			self.x_start = pix.x
			self.x_stop = pix.x
			self.initial_x = pix.x
			self.initial_y = pix.y
			self.last_y = pix.y

		#Registering pixel: Checking if this is in the correct row (either the current one or the next one)
		if(pix.y != self.last_y):
			if pix.y - self.last_y != 1:
				return False
			self.last_y = pix.y

		#Registering pixel: Checking if it is within the boundaries we have set for the row. May exceed them by 3 TODO: Check if needs to be changed.
		if pix.x < self.x_start:
			if self.x_start - pix.x > 3: #TODO: check, perhaps we can use ghosting here.
				return False
		elif pix.x > self.x_stop:
			if pix.x - self.x_stop > 3:
				return False

		if pix.x < self.x_start:
			self.x_start = pix.x
		elif pix.x > self.x_stop:
			self.x_stop = pix.x

		self.sumx += pix.intensity * (self.initial_x - pix.x)
		self.sumy += pix.intensity * (self.initial_y - pix.y)
		self.sum += pix.intensity
		self.size += 1

		return True

	def get_star(self):
		return(self.sumx/self.sum, self.sumy/self.sum, self.sum)



# This function looks at the difference between pixels. If it is smaller than the given cutoff, make them 0.
def diff(img, cutoff):
	for rida in img:
		for i in range(len(rida)-1):
			if abs(rida[i]-rida[i+1]) < cutoff:
				rida[i] = 0;

		rida[len(rida)-1] = 0

	return img

# This function looks at the size of non-0 pixel islands. If they're smaller than the size given, make them 0.
def clean(img, size):
	for rida in img:
		for i in range(len(rida)-1):
			j = 0
			for k in range(i - size, i + size + 1):
				try:
					if img[k]:
						j += 1
				except IndexError:


print("Opening file " + sys.argv[1])

img = 0

try:
	img = pyfits.getdata(sys.argv[1], ignore_missing_end = True)

except:
	print("Bad filename.") 
	exit(-1)


img = diff(img, 25)


print(img)

pyfits.writeto(sys.argv[1]+"_dy_2.fts", img, output_verify = "fix", clobber = True)
