import sys
from math import sqrt

in_file = file(sys.argv[1], "r")
out_file = file("real_avg.txt", "w")

a = []
for rida in in_file:
	a.append(int(rida))
	avg = sum(a)/len(a)
	v = []
	for i in a:
		v.append((avg - i)*(avg - i))
	var = sum(v)/len(v)
	out_file.write(rida.strip("\n") + ", " + str(avg) + ", " + str(var) + ", " + str(int(round(sqrt(var)))))
	out_file.write("\n")

