#include <stdio.h>      /* printf, fgets */
#include <stdlib.h>     /* atol */
#include <stdint.h>

uint16_t M = 0;
uint16_t oM = 0;
uint32_t S = 0;
uint16_t k = 0;

void mean(uint16_t x)
{
	k++;
	oM = M;
	M = M - (M - x)/k;
	
}

void variance(uint16_t x)
{
	mean(x);
	S = S + (x - oM)*(x - M);
}

uint32_t isqrt(uint32_t num) {
    uint32_t res = 0;
    uint32_t bit = 1 << 30; // The second-to-top bit is set.
    // "bit" starts at the highest power of four <= the argument.
    while (bit > num)
    {
        bit >>= 2;
    }

    while (bit != 0) {
        if (num >= res + bit) {
            num -= res + bit;
            res = (res >> 1) + bit;
        }
        else
            res >>= 1;
        bit >>= 2;
    }
    return res;
}

int main(int argc, char* argv[])
{
	unsigned long i;
	FILE* input_file = fopen(argv[1], "r");
	FILE* out_file = fopen(argv[2], "w");

	fprintf(out_file, "x, mean, variance, stdev\n");
	while(!feof(input_file))
	{
		fscanf(input_file, "%lu\n", &i);
		variance(i);
		fprintf(out_file, "%lu, %u, %u, %u\n", i,  M, (S) ? S/(k-1) : 0, isqrt((S) ? S / (k-1) : 0));
	}
	fclose(input_file);
	fclose(out_file);
}