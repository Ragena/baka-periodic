#include "stdio.h"
#include "stdint.h"

uint16_t X = 0xFFFF;
uint32_t X_2;

int main(int argc, char* argv[])
{
	FILE* luts = fopen("luts.vhd", "w");

	while (X)
	{
		X_2 = X * X;
		fprintf(luts, "if num > X\"%X\" then\n\tRES := X\"%X\";\nels", X_2-1, X);
		X--;
	}
	fprintf(luts, "e\n\tRES := (others => '0');\nend if;\n");
	fclose(luts);
}