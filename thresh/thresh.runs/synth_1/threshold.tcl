# 
# Synthesis run script generated by Vivado
# 

set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000
create_project -in_memory -part xc7a35tcpg236-3

set_param project.singleFileAddWarning.threshold 0
set_param project.compositeFile.enableAutoGeneration 0
set_param synth.vivado.isSynthRun true
set_property webtalk.parent_dir /home/andu/baka/thresh/thresh.cache/wt [current_project]
set_property parent.project_path /home/andu/baka/thresh/thresh.xpr [current_project]
set_property default_lib xil_defaultlib [current_project]
set_property target_language VHDL [current_project]
read_vhdl -library xil_defaultlib {
  /home/andu/baka/thresh/thresh.srcs/sources_1/imports/Undo/sqrt.vhd
  /home/andu/baka/thresh/thresh.srcs/sources_1/imports/Undo/binary_sqrt.vhd
}
foreach dcp [get_files -quiet -all *.dcp] {
  set_property used_in_implementation false $dcp
}

synth_design -top threshold -part xc7a35tcpg236-3


write_checkpoint -force -noxdef threshold.dcp

catch { report_utilization -file threshold_utilization_synth.rpt -pb threshold_utilization_synth.pb }
