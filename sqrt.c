#include <stdio.h>      /* printf, fgets */
#include <stdlib.h>     /* atol */

long isqrt(long num) {
    long res = 0;
    long bit = 1 << 14; // The second-to-top bit is set: 1 << 30 for 32 bits
    int i = 0;
    // "bit" starts at the highest power of four <= the argument.
    while (bit > num)
    {
        bit >>= 2;
	i++;
    }

    while (bit != 0) {
        if (num >= res + bit) {
            num -= res + bit;
            res = (res >> 1) + bit;
        }
        else
            res >>= 1;
        bit >>= 2;
        i++;
    }
    printf("%dit\n", i);
    return res;
}

int main(int argc, char* argv[])
{
	printf("%d\n", isqrt(atol(argv[1])));
}
