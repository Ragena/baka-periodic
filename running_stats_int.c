#include "stdio.h"
#include "stdint.h"

int64_t Pi_prim	= 0;
int64_t Pi 		= 0;
int32_t mi 		= 0;
int32_t delta_m	= 0;
int16_t Ni		= 0;
int32_t ui 		= 0;

uint32_t sigma_i_squared = 0;
uint32_t Vi				= 0;
uint32_t Wi_prim		= 0;
uint32_t Wi 			= 0;
uint32_t Wi_prim_prim	= 0;
uint32_t delta_v		= 0;

int32_t running_avg(uint16_t x)
{
	Ni++;
	Pi_prim = Pi + (x - mi);

	delta_m = Pi_prim / Ni;
	mi = mi + delta_m;

	Pi = Pi_prim - delta_m * Ni;

	ui = mi + Pi / Ni;

	return ui;
}

void running_stat(uint16_t x)
{
	Ni++;
	Pi_prim = Pi + (x - mi);

	delta_m = Pi_prim / Ni;

	Wi_prim = Wi + (x - mi) * (x - mi) - Vi;

	Wi_prim_prim = Wi_prim - 2 * delta_m * Pi_prim + Ni * delta_m * delta_m;

	delta_v = (Wi_prim_prim) ? Wi_prim_prim / (Ni-1) : 0;
	printf("%d\n", delta_v);

	Vi = Vi + delta_v;
	
	Wi = Wi_prim_prim - (Ni-1)*delta_v;

	mi = mi + delta_m;

	Pi = Pi_prim - Ni * delta_m;

	ui = mi + Pi / Ni;

	//sigma_i_squared = (Wi) ? Vi + Wi / (Ni-1) : Vi; // The result of Wi / (Ni-1) is always 0, since we don't use floating point numbers.
	sigma_i_squared = Vi;

}

uint32_t isqrt(uint32_t num) {
    uint32_t res = 0;
    uint32_t bit = 1 << 30; // The second-to-top bit is set.
    // "bit" starts at the highest power of four <= the argument.
    while (bit > num)
    {
        bit >>= 2;
    }

    while (bit != 0) {
        if (num >= res + bit) {
            num -= res + bit;
            res = (res >> 1) + bit;
        }
        else
            res >>= 1;
        bit >>= 2;
    }
    return res;
}

int main(int argc, char* argv[])
{
	unsigned long i;
	FILE* input_file = fopen(argv[1], "r");
	FILE* out_file = fopen("avgs.txt", "w");

	while(!feof(input_file))
	{
		fscanf(input_file, "%lu\n", &i);
		running_stat(i);
		fprintf(out_file, "%u, %d, %u, %u\n", i,  (int)ui, (unsigned long)sigma_i_squared, isqrt(sigma_i_squared));
	}
	fclose(input_file);
	fclose(out_file);
}