import pyfits
import sys
from myhdl import intbv

class Blob:
	blobs = []
	def __init__(self):
		self.sumx = intbv(0,min=0, max=0xFFFF00) # 16bit pixels, blobs are not larger than 4x4
		self.sumy = intbv(0,min=0, max=0xFFFF00) # 16bit pixels, blobs are not larger than 4x4
		self.sum = intbv(0,min=0, max=0xFFFF*36*6) # 16bit pixels, blobs are not larger than 4x4
		self.last_y = intbv(0,min=0)[16:]
		self.x_start = intbv(0,min=0)[16:]
		self.x_stop = intbv(0,min=0)[16:]
		self.size = intbv(0,min=0, max = 36)
		self.initial_x = intbv(0,min=0)[16:]
		self.initial_y = intbv(0,min=0)[16:]

		blobs.append(self)

	def add_pix(self, pix):

		#First add. Boundaries are wrong.
		if self.size == 0:
			self.x_start = pix.x
			self.x_stop = pix.x
			self.initial_x = pix.x
			self.initial_y = pix.y
			self.last_y = pix.y

		#Registering pixel: Checking if this is in the correct row (either the current one or the next one)
		if(pix.y != self.last_y):
			if pix.y - self.last_y != 1:
				return False
			self.last_y = pix.y

		#Registering pixel: Checking if it is within the boundaries we have set for the row. May exceed them by 3 TODO: Check if needs to be changed.
		if pix.x < self.x_start:
			if self.x_start - pix.x > 3: #TODO: check, perhaps we can use ghosting here.
				return False
		elif pix.x > self.x_stop:
			if pix.x - self.x_stop > 3:
				return False

		if pix.x < self.x_start:
			self.x_start = pix.x
		elif pix.x > self.x_stop:
			self.x_stop = pix.x

		self.sumx += pix.intensity * (pix.x - self.initial_x)
		self.sumy += pix.intensity * (pix.y - self.initial_y)
		self.sum += pix.intensity
		self.size += 1

		return True

	def get_star(self):
		if self.size >= 4
			return((self.sumx/self.sum) + self.initial_x, (self.sumy/self.sum) + self.initial_y, self.sum)

	def contains(self, pix):
		center = self.get_star()
		if abs(pix.x - center[0]) > 6:
			return False
		if abs(pix.y - center[1]) > 6:
			return False
		return True

	def combine(self, blob):
		if self == blob:
			return

		self.sumx += blob.sumx
		self.sumy += blob.sumy
		self.sum += blob.sum
		self.size += blob.size

		del blob

class Pixel:
	running_window = [0 for i in range(10)]
	# http://seat.massey.ac.nz/research/centres/SPRG/pdfs/2013_IVCNZ_214.pdf
	Pi = intbv(0, min=0)[16:]
	Pi_prim = intbv(0, min=0)[16:]
	dm = intbv(0, min=0)[16:]
	mi = intbv(0, min=0)[16:]
	ui = intbv(0, min=0)[16:]
	Ni = intbv(0, min=0)[16:]

	sigma2 = intbv(0, min=0)[16:]
	Wi = intbv(0, min=0)[16:]
	Wi_prim = intbv(0, min=0)[16:]
	Wi_prim2 = intbv(0, min=0)[16:]
	dv = intbv(0, min=0)[16:]
	Vi = intbv(0, min=0)[16:]

	Ri = intbv(0, min=0)[16:]
	Ri_prim = intbv(0, min=0)[16:]
	Si = intbv(1, min=0)[16:]
	ds = intbv(0, min=0)[16:]

	def update_statistics(self):
		Ni += 1
		Pi_prim = Pi + self.intensity - mi
		dm = intbv(Pi_prim.signed()/Ni.signed(), min=0)[16:]

		Wi_prim = Wi + ((self.intensity - mi) * (self.intensity - mi)) - Vi
		Wi_prim2 = Wi_prim - ((2 * dm) * Pi_prim) + (Ni * dm * dm)
		dv = intbv(Wi_prim2.signed()/(Ni-1), min=0)[16:]

		Ri_prim = Ri + dv
		ds =

		Vi += dv
		Wi = Wi_prim2 - ((Ni - 1) * dv)
		sigma2 = Vi + Wi

		mi += dm
		Pi -= (Ni * dm)
		ui = mi + intbv(Pi.signed()/Ni.signed(), min=0)[16:]



	def __init__(self, x, y, intensity):
		self.x = intbv(x, min=0)[16:]
		self.y = intbv(y, min=0)[16:]
		self.intensity = intbv(int(intensity), min=0)[16:]
		self.blob = None



	def window_avg(self):
		summa = 0
		for i in self.running_window:
			summa += i
		return summa/len(self.running_window)

	def add_to_window(self):
		avg = [0, 0]
		avg[0] = self.window_avg()

		del self.running_window[0]
		self.running_window.append(self.intensity)

		avg[1] = self.window_avg()

		return avg

	def combine(self, pix):
		if self.blob == None:
			self.blob = Blob()
			self.blob.add_pix(self)

		pix.blob = self.blob
		self.blob.add_pix(pix)


# This function looks at the difference between pixels. If it is smaller than the given cutoff, make them 0.
#def diff(img, cutoff):
#	for rida in img:
#		for i in range(len(rida)-1):
#			if abs(rida[i]-rida[i+1]) < cutoff:
#				rida[i] = 0;
#
#		rida[len(rida)-1] = 0
#
#	return img
#
## This function looks at the size of non-0 pixel islands. If they're smaller than the size given, make them 0.
#def clean(img, size):
#	for rida in img:
#		for i in range(len(rida)-1):
#			j = 0
#			for k in range(i - size, i + size + 1):
#				try:
#					if img[k]:
#						j += 1
#				except IndexError:
#

print("Opening file " + sys.argv[1])

img = 0

try:
	img = pyfits.getdata(sys.argv[1], ignore_missing_end = True)
except:
	print("Bad filename.")
	exit(-1)

print("Parsing image.")

blobs = []

for i in range(len(img)):
	for j in range(len(img[i])):
		#pixel values.
		pix = Pixel(i, j, img[i][j])
		if img[i][j] - pix.add_to_window()[0] < 100:
			img[i][j] = 0

		if(img[i][j]):
			if i > 0:
				if(img[i][j])
		#img[i][j] = pix.add_to_window()[1]

pyfits.writeto(sys.argv[1]+"_dy_1.fts", img, output_verify = "fix", clobber = True)
